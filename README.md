# Contao Mailjet Bundle

## About
With this Bundle, you can add a custom opt-in, based on Contao forms, with an opt-in log and domain-dns lookups against SPAM.
More to Mailjet: https://www.mailjet.de/

This bundle also contains a massmailing function based on Excel-Files and Mailjet Transactional E-Mails.
So you can bulk send e-mails - also multiple times to the same e-mail address.

## Installation
Install [composer](https://getcomposer.org) if you haven't already.
Add the unlisted Repo (not on packagist.org) to your composer.json:
```
"repositories": [
  {
    "type": "vcs",
    "url" : "https://bitbucket.org/memo_development/contao-mailjet-bundle.git"
  }
],
```

Add the bundle to your requirements:
```
"memo_development/contao-mailjet-bundle": "^2.0",
```

## Usage (German)
1. Konto anlegen (API und Zugang gem. Checkliste im Asana)
2. Transaktionales Bestätigungsmail erstellen (Beispiel Acasa Suites oder NeoVac) aus Vorlage (z.B. wie im Opt-In-DE.mjml)
3. Kontaktfelder anlegen (firstname, lastname, salutation, etc.)
4. Kontaktliste anlegen (evtl. Import?)
5. Seiten anlegen
    1. Anfrage-Bestätigung von Newsletter-Formular (mit Hinweis auf Opt-In E-Mail, jenes "bald kommt")
    2. Anmeldung-Bestätigung (wird nachher im Opt-In Mail verlinkt, hier platziert man das "Mailjet Opt-In Element")
6. Formular erstellen (mindestens das Feld "email" und alle anderen Felder anlegen - Text, Checkbox, Radio oder Select gehen) und einer Weiterleitung auf die "Anfrage-Bestätigung" Seite von vorhin
7. Bundle in Seite einbauen und einrichten
    1. API anlegen (SMTP Daten aus mailjet.de holen)
    2. Templates und Kontaktlisten synchen
    2. Integration anlegen (Daten aus verschiedenen Portalen nötig, gesynchte Kontaktlisten/Templates aus MailJet, Formular/Seiten aus Contao)
    3. Module (Registrierungsforumlar und Opt-In Element platzieren)
8. Testen

## Requirements
* Contao 5.2 or higher
* PHP 8.1 or higher

## Contribution
Bug reports and pull requests are welcome
