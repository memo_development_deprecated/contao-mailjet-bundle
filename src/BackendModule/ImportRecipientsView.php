<?php
/**
 * @package   MailJetBundle
 * @author    Rory Zünd, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */


namespace Memo\MailJetBundle\BackendModule;

class ImportRecipientsView extends \Contao\BackendModule
{
    protected $strTemplate = 'be_recipient_import_view';

    public function compile()
    {
        $this->Template->strTemplate = $this->strTemplate;
    }
}
