<?php
/**
 * @package   MailJetBundle
 * @author    Rory Zünd, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

namespace Memo\MailJetBundle\Elements;

use Memo\MailJetBundle\Model\MailJetRecipientsArchivModel;
use Memo\MailJetBundle\Model\MailJetRecipientsModel;
use Contao\ContentElement;
use Contao\Input;

class MailJetMassmailOptOutElement extends ContentElement
{
    protected $strTemplate = 'ce_mailjet_massmail_optout';

    public function generate()
    {
        return parent::generate();
    }

    /**
     * Generates the content element.
     */
    protected function compile()
    {
        global $objPage;

        $strEmail = Input::get('mail');
        $strHash = Input::get('hash');
        $strOptout = Input::get('optout');

        if ($strOptout == 1) {

            $this->Template->intStep = 2;

            if ($strEmail != '' && $strHash != '') {

                // Empfänger Eintrag finden
                $objRecipient = MailJetRecipientsModel::findOneBy(array('url_email=?', 'hash=?'), array($strEmail, $strHash));

                // Abmeldung nicht erfolgreich
                if(!$objRecipient) {
                    $this->Template->bolResponse = false;
                    return;
                }

                // Get the recipient archive
                $objRecipientArchiv = MailJetRecipientsArchivModel::findByPk($objRecipient->pid);

                // Abmeldung nicht erfolgreich
                if(!$objRecipientArchiv) {
                    $this->Template->bolResponse = false;
                    return;
                }

                if ($objRecipient->published == 1) {

                    // Opt-Out setzen
                    $objRecipient->published = 0;
                    $objRecipient->save();

                    // Template Meldung definieren
                    $this->Template->bolResponse = true;

                    // E-Mail schicken
                    $arrTokens = [
                        'form_person_name' => $objRecipient->firstname . " " . $objRecipient->lastname,
                        'form_person_email' => $objRecipient->email,
                        'form_person_group' => $objRecipientArchiv->title
                    ];

                    // @TODO - find a way to send the notification (was Notification Center)
                    /*$objNotification = \NotificationCenter\Model\Notification::findByPk('6');
                    if (null !== $objNotification) {
                        $objNotification->send($arrTokens);
                    }*/

                } else {

                    // Abmeldung bereits durchgeführt (aber wir bestätigen es natürlich gerne nochmals)
                    $this->Template->bolResponse = true;

                }

            } else {

                // Abmeldung nicht erfolgreich
                $this->Template->bolResponse = false;

            }

        } else {

            $this->Template->intStep = 1;

        }
    }
}
