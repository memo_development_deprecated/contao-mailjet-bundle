<?php
/**
 * @package   MailJetBundle
 * @author    Rory Zünd, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

namespace Memo\MailJetBundle\Elements;

use Codefog\HasteBundle\Form\Form;
use Contao\BackendTemplate;
use Contao\ContentElement;
use Contao\Controller;
use Contao\FormFieldModel;
use Contao\FormModel;
use Contao\Input;
use Contao\System;
use Memo\MailJetBundle\Model\MailJetAPIModel;
use Memo\MailJetBundle\Model\MailJetContactlistModel;
use Memo\MailJetBundle\Model\MailJetIntegrationModel;
use Memo\MailJetBundle\Model\MailJetOptinModel;
use Memo\MailJetBundle\Service\MailJetHandler;
use Symfony\Component\HttpFoundation\Request;


class MailJetRegistrationElement extends ContentElement
{
    protected $strTemplate = 'ce_mailjet_registration';

    public function generate()
    {

        if (System::getContainer()->get('contao.routing.scope_matcher')->isBackendRequest(System::getContainer()->get('request_stack')->getCurrentRequest() ?? Request::create(''))) {


            $template = new BackendTemplate('be_wildcard');

            if ($objIntegration = MailJetIntegrationModel::findByPk($this->mailjet_integration_id)) {
                $template->wildcard = $objIntegration->title;
            } else {
                $template->wildcard = 'ID: ' . $this->mailjet_integration_id;
            }

            return $template->parse();

        }

        return parent::generate();
    }

    /**
     * Generates the content element.
     */
    protected function compile()
    {
        global $objPage;

        // MailJet Form ID gesetzt?
        if ($objIntegration = MailJetIntegrationModel::findByPk($this->mailjet_integration_id)) {

            // MailJet API gesetzt?
            if ($objAPI = MailJetAPIModel::findByPk($objIntegration->api_id)) {

                // Formular existiert?
                if ($objForm = FormModel::findByPk($objIntegration->form_id)) {

                    //Haste Formular generieren
                    $objHasteForm = new Form('mailjet_form', 'POST', function ($objHaste) {
                        return Input::post('FORM_SUBMIT') === $objHaste->getFormId();
                    });

                    // Formular aus Formulargenerator auslesen
                    $objHasteForm->addFieldsFromFormGenerator($objForm->id, function (&$strField, &$arrDca) use ($objIntegration) {

                        // Set name for the mailjet_contactlists field
                        if ($arrDca['type'] == "mailjet_contactlists") {
                            if ($objIntegration->contactlisttype == 'select') {
                                $strField = "mailjet_contactlists";
                                $arrDca['name'] = "mailjet_contactlists";
                            } else {
                                return false;
                            }

                        }

                        // Alle Felder (ohne Anpassung) hinzufügen
                        return true;

                    });

                    // Formular wurde abgeschickt
                    if ($objHasteForm->validate()) {

                        // Daten aus Formular auslesen
                        $arrData = $objHasteForm->fetchAll();

                        // Zusatzdaten ermitteln
                        $arrPropertyData = array();
                        if ($collFormFields = FormFieldModel::findByPid($objForm->id)) {

                            foreach ($collFormFields as $objFormfield) {

                                if ($objFormfield->mailjetfield && $objFormfield->type != 'mailjet_contactlists') {

                                    if ($objFormfield->mailjetfieldname) {
                                        $arrPropertyData[$objFormfield->mailjetfieldname] = $arrData[$objFormfield->name];
                                    } else {
                                        $arrPropertyData[$objFormfield->name] = $arrData[$objFormfield->name];
                                    }

                                }
                            }
                        }

                        // Kontakt erstellen
                        if ($arrData['email'] != '') {

                            // Get Contactlists
                            $arrContactLists = array();
                            $arrContactListsIDs = array();
                            if ($objIntegration->contactlisttype == 'select') {

                                if (is_array($arrData['mailjet_contactlists']) && count($arrData['mailjet_contactlists']) > 0) {
                                    foreach ($arrData['mailjet_contactlists'] as $intContactlistID) {
                                        if ($objContactlist = MailJetContactlistModel::findByPk($intContactlistID)) {
                                            $arrContactLists[] = $objContactlist->contactlist_id;
                                            $arrContactListsIDs[] = $objContactlist->id;
                                        }

                                    }
                                }

                            } else {

                                if ($objContactlist = MailJetContactlistModel::findByPk($objIntegration->contactlist_id)) {
                                    $arrContactLists[] = $objContactlist->contactlist_id;
                                    $arrContactListsIDs[] = $objContactlist->id;
                                }
                            }

                            // E-mail checken
                            if (MailJetHandler::checkMailAdress($arrData['email'], true)) {

                                if (MailJetHandler::addContact($objAPI, $arrData['email'])) {

                                    $objOptIn = new MailJetOptinModel();
                                    $strOptinType = 'double';

                                    switch($objIntegration->type){
                                        case 'with_optin':
                                            $strOptinType = 'double';
                                            break;
                                        case 'with_optin_secure':
                                            $strOptinType = 'triple';
                                            break;
                                        case 'without_optin':
                                            $strOptinType = 'none';
                                            break;
                                    }

                                    $objOptIn->newOptIn($arrData['email'], $arrPropertyData, $objIntegration->id, $arrContactListsIDs, $strOptinType);

                                    if ($objIntegration->type == 'without_optin') {

                                        // Daten vom Optin holen
                                        $arrPropertyData = unserialize($objOptIn->formfields);

                                        // Gibt es zusätzliche Attribute?
                                        if (count($arrPropertyData) > 1) {
                                            MailJetHandler::addContactProperty($objAPI, $objOptIn, $objIntegration, $arrPropertyData);
                                        }

                                        // Fall: keine Kontaktlisten definiert / ausgewaehlt
                                        if (!$arrContactListsIDs || count($arrContactListsIDs) == 0) {

                                            if ($objIntegration->contactlisttype == 'select') {
                                                MailJetHandler::removeContactFromAllLists($objAPI, $arrData['email']);
                                            } else {
                                                throw new \Exception('No or wrong contactlist was selected - no optin possible');
                                            }

                                        } else {

                                            // Wenn es Kontaktlisten hat, aber die selbstauswahl gewaehlt wurde, zuerst von allen anderen Listen abmelden
                                            if ($objIntegration->contactlisttype == 'select') {
                                                MailJetHandler::removeContactFromAllLists($objAPI, $arrData['email']);
                                            }

                                            // Kontakt in Kontaktliste hinzufuegen
                                            foreach ($arrContactLists as $intContactList) {
                                                if (!MailJetHandler::addContactToList($objAPI, $intContactList, $arrData['email'])) {
                                                    throw new \Exception("Could not add contact " . $arrData['email'] . " to contactlist-ID: " . $intContactList);
                                                }
                                            }
                                        }

                                        $objOptIn->setDoubleOptIn();

                                    } else {

                                        $objOptIn->sendOptIn();

                                    }

                                    // Weiterleiten auf Bestätigungsseite
                                    if ($objForm->jumpTo) {

                                        $strURL = System::getContainer()->get('contao.insert_tag.parser')->replace('{{link_url::' . $objForm->jumpTo . '}}');

                                        if (!$strURL) {
                                            throw new \Exception("Page with the follow ID does not exist: " . $objForm->jumpTo);
                                        }

                                        Controller::redirect($strURL, 302);
                                        exit();

                                    } else {

                                        if ($objIntegration->type == 'without_optin') {

                                            // Formular HTML generieren
                                            $strForm = $objHasteForm->generate();

                                            // Formular an Template übergeben
                                            $this->Template->htmlForm = $strForm;
                                            $this->Template->htmlForm .= "<p class='success'>" . $GLOBALS['TL_LANG']['mailjet']['optin-success-title'] . "</p><br>";


                                        } else {
                                            $this->Template->htmlForm = $GLOBALS['TL_LANG']['mailjet']['optin-confirm-text'];
                                        }

                                    }

                                } else {

                                    // Formular HTML generieren
                                    $strForm = $objHasteForm->generate();

                                    // Formular an Template übergeben
                                    $this->Template->htmlForm = "<p class='error'>" . $GLOBALS['TL_LANG']['mailjet']['form-fail-text'] . "</p><br>";
                                    $this->Template->htmlForm .= $strForm;

                                }

                            } else {

                                // Formular HTML generieren
                                $strForm = $objHasteForm->generate();

                                // Formular an Template übergeben
                                $this->Template->htmlForm = "<p class='error'>" . $GLOBALS['TL_LANG']['mailjet']['wrong-email'] . "</p><br>";
                                $this->Template->htmlForm .= $strForm;

                            }
                        } else {

                            // Formular HTML generieren
                            $strForm = $objHasteForm->generate();

                            // Formular an Template übergeben
                            $this->Template->htmlForm = "<p class='error'>" . $GLOBALS['TL_LANG']['mailjet']['no-email'] . "</p><br>";
                            $this->Template->htmlForm .= $strForm;

                        }

                    } else {

                        // Formular HTML generieren
                        $strForm = $objHasteForm->generate();

                        // Klasse auslesen und mitgeben
                        $arrAttribute = unserialize($objForm->attributes);
                        if ($arrAttribute && is_array($arrAttribute) && array_key_exists(1, $arrAttribute)) {

                            $strClass = $arrAttribute[1];
                            $intPos = strpos($strForm, 'method="');
                            if ($intPos !== false) {
                                $strForm = substr_replace($strForm, 'class="' . $strClass . '" method="', $intPos, strlen('method="'));
                            }
                        }

                        // Formular an Template übergeben
                        $this->Template->htmlForm = $strForm;
                    }

                } else {

                    throw new \Exception("Form does not exist: " . $objAPI->form_id);

                }

            } else {

                throw new \Exception("MailJet API does not exist: " . $objIntegration->api_id);

            }

        } else {

            throw new \Exception("MailJet Form does not exist: " . $this->mailjet_integration_id);

        }

    }
}
