<?php
/**
 * @package   MailJetBundle
 * @author    Rory Zünd, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */


namespace Memo\MailJetBundle\Elements;

use Contao\ContentElement;
use Contao\Input;
use Memo\MailJetBundle\Model\MailJetAPIModel;
use Memo\MailJetBundle\Model\MailJetContactlistModel;
use Memo\MailJetBundle\Model\MailJetIntegrationModel;
use Memo\MailJetBundle\Model\MailJetOptinModel;
use Memo\MailJetBundle\Service\MailJetHandler;
use Codefog\HasteBundle\Form\Form;

class MailJetOptInElement extends ContentElement
{
    protected $strTemplate = 'ce_mailjet_optin';

    public function generate()
    {
        return parent::generate();
    }

    /**
     * Generates the content element.
     */
    protected function compile()
    {
        global $objPage;

        $strEmail = Input::get('mail');
        $strHash = Input::get('hash');

        // Opt-In Eintrag finden
        $objOptin = MailJetOptinModel::findOneBy(array('email_url=?', 'hash=?'), array($strEmail, $strHash));

        if (!$objOptin) {
            $objOptin = MailJetOptinModel::findOneBy(array('email_url=?', 'hash=?'), array(urlencode($strEmail), $strHash));
        }

        if ($objOptin) {

            // Link geöffnet vermerken
            $objOptin->setOpened();

            // Based on the Opt-In Type, we need to do different things

            // Case 1 and 2 - No Opt-In or Opt-In by opening the current url, in both cases, set the opt-in as activated
            if($objOptin->optin_type == 'none' || $objOptin->optin_type == 'double') {
                self::optIn($objOptin);
                return true;
            }

            // Case 2 - Triple Opt-In, Opt-In by clicking the button

            // Generate Opt-In Button
            $objForm = new Form('optinform', 'POST', function ($objHaste) {
                return Input::post('FORM_SUBMIT') === $objHaste->getFormId();
            });
            $objForm->preserveGetParameters();
            if ($this->mailjet_optin_button_text != '') {
                $strLabel = $this->mailjet_optin_button_text;
            } else {
                $strLabel = "Newsletter abonnieren";
            }
            $objForm->addFormField('submit', array(
                'label' => $strLabel,
                'inputType' => 'submit'
            ));
            $objForm->addContaoHiddenFields();

            // Form was submitted - Double-Opt-In complete
            if ($objForm->validate()) {

                self::optIn($objOptin);

            } else {

                $this->Template->strForm = $objForm->generate();

            }

        } else {

            // Template Meldung definieren
            $this->Template->bolResponse = false;
        }
    }

    public function optIn($objOptin) {

        // Mehr Informationen für Kontaktlisten Add ermitteln
        $objIntegration = MailJetIntegrationModel::findByPk($objOptin->mailjet_integration_id);

        if(!$objIntegration) {
            throw new \Exception('No integration found for id: ' . $objOptin->mailjet_integration_id . ' - no optin possible');
        }

        $objAPI = MailJetAPIModel::findByPk($objIntegration->api_id);

        if(!$objAPI) {
            throw new \Exception('No API found for id: ' . $objIntegration->api_id . ' - no optin possible');
        }

        // Get Contactlists
        $arrContactLists = array();
        if ($objIntegration->contactlisttype == 'select' && $objOptin->contactlists) {
            $arrContactListIDs = unserialize($objOptin->contactlists);

            if (is_array($arrContactListIDs) && count($arrContactListIDs) > 0) {
                foreach ($arrContactListIDs as $intContactlistID) {
                    if ($objContactlist = MailJetContactlistModel::findByPk($intContactlistID)) {
                        $arrContactLists[] = $objContactlist->contactlist_id;
                    }
                }
            }
        } elseif ($objIntegration->contactlisttype == 'preset') {
            if ($objContactlist = MailJetContactlistModel::findByPk($objIntegration->contactlist_id)) {
                $arrContactLists[] = $objContactlist->contactlist_id;
            }
        }

        // Kontakt in Kontaktliste hinzufuegen
        $arrPropertyData = unserialize($objOptin->formfields);
        $bolResult = true;

        // Gibt es zusätzliche Attribute?
        if (count($arrPropertyData) > 1) {
            MailJetHandler::addContactProperty($objAPI, $objOptin, $objIntegration, $arrPropertyData);
        }

        // Wenn keine Kontaktliste ausgewählt wurde
        if (!$arrContactLists || count($arrContactLists) == 0) {

            // Und der typ der Anmeldung die eigene Auswahl ist
            if ($objIntegration->contactlisttype == 'select') {

                // Person von allen Kontaktlisten abmelden
                MailJetHandler::removeContactFromAllLists($objAPI, $objOptin->email);
            } else {

                // Ansonsten ist das ein Fehler - man muesste als Admin eine Kontaktliste ind er Integration auswaehlen
                throw new \Exception('No or wrong contactlist was selected - no optin possible');
            }

        } else {

            // Wenn es Kontaktlisten hat, aber die selbstauswahl gewaehlt wurde, zuerst von allen anderen Listen abmelden
            if ($objIntegration->contactlisttype == 'select') {
                MailJetHandler::removeContactFromAllLists($objAPI, $objOptin->email);
            }

            // Alle Kontaktlisten anmelden
            foreach ($arrContactLists as $intContactList) {
                if (!MailJetHandler::addContactToList($objAPI, $intContactList, $objOptin->email)) {
                    $bolResult = false;
                }
            }

        }

        // Template Meldung definieren
        $this->Template->bolResponse = $bolResult;

        // Double Opt-In setzen
        $objOptin->setDoubleOptIn();

    }
}
