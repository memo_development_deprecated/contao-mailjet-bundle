<?php
/**
 * @package   MailJetBundle
 * @author    Rory Zünd, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

namespace Memo\MailJetBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class MemoMailJetBundle extends Bundle
{
}
