<?php
/**
 * @package   MailJetBundle
 * @author    Rory Zünd, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

namespace Memo\MailJetBundle\Model;


/**
 * Class MailJetRecipientsArchivModel
 *
 * Reads and writes MailJetRecipientsArchivModel.
 */
class MailJetRecipientsArchivModel extends BaseModel
{
    /**
     * Table name
     * @var string
     **/
    protected static $strTable = 'tl_mailjet_recipients_archiv';

}
