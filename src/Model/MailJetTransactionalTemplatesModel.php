<?php
/**
 * @package   MailJetBundle
 * @author    Rory Zünd, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

namespace Memo\MailJetBundle\Model;

/**
 * Class MailJetTransactionalTemplatesModel
 *
 * Reads and writes MailJetAPIModel.
 */
class MailJetTransactionalTemplatesModel extends BaseModel
{
    /**
     * Table name
     * @var string
     **/
    protected static $strTable = 'tl_mailjet_transactional_templates';

}
