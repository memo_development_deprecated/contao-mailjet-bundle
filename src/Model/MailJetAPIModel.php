<?php
/**
 * @package   MailJetBundle
 * @author    Rory Zünd, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

namespace Memo\MailJetBundle\Model;

/**
 * Class MailJetAPIModel
 *
 * Reads and writes MailJetAPIModel.
 */
class MailJetAPIModel extends BaseModel
{
    /**
     * Table name
     * @var string
     **/
    protected static $strTable = 'tl_mailjet_api';

}
