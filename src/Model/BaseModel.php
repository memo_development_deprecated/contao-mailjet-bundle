<?php
/**
 * @package   MailJetBundle
 * @author    Rory Zünd, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

namespace Memo\MailJetBundle\Model;

use Codefog\HasteBundle\Model\DcaRelationsModel;

/**
 * Class MailJetBundle
 *
 * Erweiterung der Model Klasse.
 */
class BaseModel extends DcaRelationsModel
{
    public function getLanguageModel($strLanguage)
    {

        if ($strLanguage == 'de') {
            $strPostfix = "";
        } else {
            $strPostfix = $this->getActiveLanguagePostfix($strLanguage);
        }

        foreach ($this->arrData as $key => $value) {


            $strLanguageField = $key . $strPostfix;

            if ($strPostfix !== '') {
                if (array_key_exists($strLanguageField, $this->arrData) && $this->$strLanguageField !== '' && $this->$strLanguageField !== null) {
                    $this->$key = $this->$strLanguageField;
                }
            }

        }

        return $this;
    }

    public function getActiveLanguagePostfix($strLanguage)
    {
        switch ($strLanguage) {
            case "de-CH":
            case "de":
            case "de_CH":
            case "DE":
                $strLanguageFilter = 'de';
                $strPostfix = '_de';
                break;
            case "fr-CH":
            case "fr":
            case "fr_CH":
            case "FR":
                $strLanguageFilter = 'fr';
                $strPostfix = '_fr';
                break;
            case "it-CH":
            case "it":
            case "it_CH":
            case "IT":
                $strLanguageFilter = 'it';
                $strPostfix = '_it';
                break;
            default:
                $strLanguageFilter = 'en';
                $strPostfix = '';
                break;
        }
        return $strPostfix;
    }
}
