<?php
/**
 * @package   MailJetBundle
 * @author    Rory Zünd, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

namespace Memo\MailJetBundle\Model;

/**
 * Class MailJetContactlistModel
 *
 * Reads and writes MailJetContactlistModel.
 */
class MailJetContactlistModel extends BaseModel
{
    /**
     * Table name
     * @var string
     **/
    protected static $strTable = 'tl_mailjet_contactlists';

}
