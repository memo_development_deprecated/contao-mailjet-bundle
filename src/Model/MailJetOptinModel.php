<?php
/**
 * @package   MailJetBundle
 * @author    Rory Zünd, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

namespace Memo\MailJetBundle\Model;

use Contao\System;
use Memo\MailJetBundle\Service\MailJetHandler;

/**
 * Class MailJetOptinModel
 *
 * Reads and writes MailJetOptinModel.
 */
class MailJetOptinModel extends BaseModel
{
    /**
     * Table name
     * @var string
     **/
    protected static $strTable = 'tl_mailjet_optin';


    public function newOptIn($strEmail, $arrPropertyData, $intMailJetIntegrationID, $arrContactListsIDs = false, $strOptInType = 'double')
    {

        $this->mailjet_integration_id = $intMailJetIntegrationID;
        $this->tstamp = time();
        $this->email = $strEmail;
        $this->email_url = urlencode(str_replace('@', '-', $this->email));
        $this->formfields = serialize($arrPropertyData);
        $this->hash = md5($strEmail . time() . 'mailjet-0pt1n-Code');
        $this->registry_tstamp = time();
        $this->sent = 0;
        $this->sent_tstamp = 0;
        $this->activated = 0;
        $this->activated_tstamp = 0;
        $this->optin_type = $strOptInType;
        if ($arrContactListsIDs) {
            $this->contactlists = serialize($arrContactListsIDs);
        }
        $this->save();

    }

    public function sendOptIn()
    {

        if (!$this->sent) {

            // Daten aufbereiten
            $objIntegration = MailJetIntegrationModel::findByPk($this->mailjet_integration_id);

            if (!$objIntegration) {
                throw new \Exception('Integration not found with id: ' . $this->mailjet_integration_id);
            }

            $objAPI = MailJetAPIModel::findByPk($objIntegration->api_id);

            if (!$objAPI) {
                throw new \Exception('API not found with id: ' . $objIntegration->api_id);
            }

            // OptIn URL generieren
            $strPageURL = System::getContainer()->get('contao.insert_tag.parser')->replace($objIntegration->opt_in_page);

            $strURL = $strPageURL . "?mail=" . $this->email_url . "&hash=" . $this->hash;
            if (!stristr($strPageURL, 'http')) {

                $strURL = rtrim(System::getContainer()->get('contao.insert_tag.parser')->replace('{{env::path}}'), '/') . $strURL;
            }

            // OptIn Mail verschicken
            MailJetHandler::sendOptInMail($this, $objAPI, $objIntegration, $strURL);

            $this->__set('sent', 1);
            $this->sent_tstamp = time();
            $this->save();

        } else {

            return false;

        }

    }

    public function setDoubleOptIn()
    {

        $this->__set('activated', 1);
        $this->__set('activated_tstamp', time());

        $this->save();

    }

    public function setOpened()
    {

        $this->__set('opened', 1);
        $this->__set('opened_tstamp', time());
        $this->__set('activated_ip', $_SERVER['REMOTE_ADDR']);
        $this->__set('activated_useragent', $_SERVER['HTTP_USER_AGENT']);

        $this->save();

    }

}
