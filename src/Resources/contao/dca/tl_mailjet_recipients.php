<?php
/**
 * @package   MailJetBundle
 * @author    Rory Zünd, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

use Contao\Input;
use Contao\Image;
use Contao\Backend;
use Contao\DataContainer;
use Contao\StringUtil;
use Contao\Versions;
use Contao\System;

/**
 * Table tl_mailjet_recipients
 */
$GLOBALS['TL_DCA']['tl_mailjet_recipients'] = array
(

    // Config
    'config' => array
    (
        'dataContainer' => Contao\DC_Table::class,
        'ptable' => 'tl_mailjet_recipients_archiv',
        'enableVersioning' => true,
        'sql' => array
        (
            'keys' => array
            (
                'id' => 'primary'
            )
        )
    ),

    // List
    'list' => array
    (
        'sorting' => array
        (
            'mode' => 2,
            'fields' => array('lastname'),
            'flag' => 1,
            'panelLayout' => 'search, filter; sort, limit'
        ),
        'label' => array
        (
            'fields' => array('lastname', 'firstname', 'email'),
            'format' => '%s %s - %s',
        ),
        'global_operations' => array
        (
            'all' => array
            (
                'label' => &$GLOBALS['TL_LANG']['MSC']['all'],
                'href' => 'act=select',
                'class' => 'header_edit_all',
                'attributes' => 'onclick="Backend.getScrollOffset();" accesskey="e"'
            ),
            'import' => array
            (
                'label' => &$GLOBALS['TL_LANG']['MSC']['import'],
                'href' => 'key=import',
                'class' => 'header_css_import',
                'attributes' => 'onclick="Backend.getScrollOffset();" accesskey="e"'
            )
        ),
        'operations' => array
        (
            'edit' => array
            (
                'label' => &$GLOBALS['TL_LANG']['tl_mailjet_recipients']['edit'],
                'href' => 'act=edit',
                'icon' => 'edit.gif'
            ),
            'copy' => array
            (
                'label' => &$GLOBALS['TL_LANG']['tl_mailjet_recipients']['copy'],
                'href' => 'act=copy',
                'icon' => 'copy.gif'
            ),
            'delete' => array
            (
                'label' => &$GLOBALS['TL_LANG']['tl_mailjet_recipients']['delete'],
                'href' => 'act=delete',
                'icon' => 'delete.gif',
                'attributes' => 'onclick="if(!confirm(\'' . ($GLOBALS['TL_LANG']['MSC']['deleteConfirm'] ?? null) . '\'))return false;Backend.getScrollOffset()"'
            ),
            'toggle' => array
            (
                'label' => &$GLOBALS['TL_LANG']['tl_mailjet_recipients']['toggle'],
                'icon' => 'visible.gif',
                'attributes' => 'onclick="Backend.getScrollOffset();return AjaxRequest.toggleVisibility(this,%s)"',
                'button_callback' => array('tl_mailjet_recipients_memo', 'toggleIcon')
            ),
            'show' => array
            (
                'label' => &$GLOBALS['TL_LANG']['tl_mailjet_recipients']['show'],
                'href' => 'act=show',
                'icon' => 'show.gif'
            ),
        )
    ),

    // Select
    'select' => array
    (
        'buttons_callback' => array()
    ),

    // Edit
    'edit' => array
    (
        'buttons_callback' => array()
    ),

    // Palettes
    'palettes' => array
    (
        'default' => '{general_legend}, firstname, lastname, email;{variable_legend}, variable1, variable2, variable3, variable4, variable5, variable6, variable7, variable8, variable9, variable10;{expert_legend}, url_email, hash, sent, published;',
    ),

    // Subpalettes
    'subpalettes' => array
    (
        '' => '',
    ),

    // Fields
    'fields' => array
    (
        'id' => array
        (
            'sql' => "int(10) unsigned NOT NULL auto_increment"
        ),
        'pid' => array
        (
            'foreignKey' => 'tl_mailjet_recipients_archiv.id',
            'relation' => array('type' => 'belongsTo', 'load' => 'lazy'),
            'sql' => "int(10) unsigned NOT NULL "
        ),
        'tstamp' => array
        (
            'default' => time(),
            'sql' => "int(10) unsigned NOT NULL DEFAULT CURRENT_TIMESTAMP"
        ),
        'tstamp_sent' => array
        (
            'sql' => "int(10) unsigned NOT NULL default '0'"
        ),
        'firstname' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_recipients']['firstname'],
            'exclude' => true,
            'filter' => false,
            'search' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => array('mandatory' => true, 'maxlength' => 255, 'tl_class' => 'w50'),
            'sql' => "varchar(255) NOT NULL default ''",
        ),
        'lastname' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_recipients']['lastname'],
            'exclude' => true,
            'filter' => false,
            'search' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => array('mandatory' => true, 'maxlength' => 255, 'tl_class' => 'w50'),
            'sql' => "varchar(255) NOT NULL default ''",
        ),
        'email' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_recipients']['email'],
            'exclude' => true,
            'filter' => false,
            'search' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => array('mandatory' => true, 'maxlength' => 255, 'tl_class' => 'w50 clr', 'rgxp' => 'email'),
            'sql' => "varchar(255) NOT NULL default ''",
        ),
        'url_email' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_recipients']['url_email'],
            'exclude' => true,
            'filter' => false,
            'search' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => array('mandatory' => false, 'maxlength' => 255, 'tl_class' => 'clr', 'readonly' => true, 'doNotCopy' => true),
            'sql' => "varchar(255) NOT NULL default ''",
        ),
        'hash' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_recipients']['hash'],
            'exclude' => true,
            'filter' => false,
            'search' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => array('mandatory' => false, 'maxlength' => 255, 'tl_class' => 'clr', 'readonly' => true, 'doNotCopy' => true),
            'sql' => "varchar(255) NOT NULL default ''",
        ),
        'variable1' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_recipients']['variable1'],
            'exclude' => true,
            'filter' => false,
            'search' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => array('mandatory' => false, 'maxlength' => 255, 'tl_class' => 'long clr'),
            'sql' => "varchar(255) NOT NULL default ''",
        ),
        'variable2' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_recipients']['variable2'],
            'exclude' => true,
            'filter' => false,
            'search' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => array('mandatory' => false, 'maxlength' => 255, 'tl_class' => 'long clr'),
            'sql' => "varchar(255) NOT NULL default ''",
        ),
        'variable3' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_recipients']['variable3'],
            'exclude' => true,
            'filter' => false,
            'search' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => array('mandatory' => false, 'maxlength' => 255, 'tl_class' => 'long clr'),
            'sql' => "varchar(255) NOT NULL default ''",
        ),
        'variable4' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_recipients']['variable4'],
            'exclude' => true,
            'filter' => false,
            'search' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => array('mandatory' => false, 'maxlength' => 255, 'tl_class' => 'long clr'),
            'sql' => "varchar(255) NOT NULL default ''",
        ),
        'variable5' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_recipients']['variable5'],
            'exclude' => true,
            'filter' => false,
            'search' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => array('mandatory' => false, 'maxlength' => 255, 'tl_class' => 'long clr'),
            'sql' => "varchar(255) NOT NULL default ''",
        ),
        'variable6' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_recipients']['variable6'],
            'exclude' => true,
            'filter' => false,
            'search' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => array('mandatory' => false, 'maxlength' => 255, 'tl_class' => 'long clr'),
            'sql' => "varchar(255) NOT NULL default ''",
        ),
        'variable7' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_recipients']['variable7'],
            'exclude' => true,
            'filter' => false,
            'search' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => array('mandatory' => false, 'maxlength' => 255, 'tl_class' => 'long clr'),
            'sql' => "varchar(255) NOT NULL default ''",
        ),
        'variable8' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_recipients']['variable8'],
            'exclude' => true,
            'filter' => false,
            'search' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => array('mandatory' => false, 'maxlength' => 255, 'tl_class' => 'long clr'),
            'sql' => "varchar(255) NOT NULL default ''",
        ),
        'variable9' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_recipients']['variable9'],
            'exclude' => true,
            'filter' => false,
            'search' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => array('mandatory' => false, 'maxlength' => 255, 'tl_class' => 'long clr'),
            'sql' => "varchar(255) NOT NULL default ''",
        ),
        'variable10' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_recipients']['variable10'],
            'exclude' => true,
            'filter' => false,
            'search' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => array('mandatory' => false, 'maxlength' => 255, 'tl_class' => 'long clr'),
            'sql' => "varchar(255) NOT NULL default ''",
        ),
        'sent' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_recipients']['sent'],
            'exclude' => true,
            'filter' => true,
            'search' => false,
            'sorting' => false,
            'inputType' => 'checkbox',
            'eval' => array('tl_class' => 'w50 clr'),
            'sql' => "char(1) NOT NULL default '0'",
        ),
        'published' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_recipients']['published'],
            'exclude' => true,
            'filter' => true,
            'search' => false,
            'sorting' => false,
            'inputType' => 'checkbox',
            'eval' => array('tl_class' => 'w50 clr'),
            'sql' => "char(1) NOT NULL default '1'",
        ),
    )
);


/**
 * Class tl_mailjet_recipients_memo
 * Definition der Callback-Funktionen für das Datengefäss.
 */
class tl_mailjet_recipients_memo extends Backend
{

    /**
     * Return the "toggle visibility" button
     *
     * @param array $row
     * @param string $href
     * @param string $label
     * @param string $title
     * @param string $icon
     * @param string $attributes
     *
     * @return string
     */
    public function toggleIcon($row, $href, $label, $title, $icon, $attributes)
    {
        if (strlen(Input::get('tid'))) {
            $this->toggleVisibility(Input::get('tid'), (Input::get('state') == 1), (@func_get_arg(12) ?: null));
            $this->redirect($this->getReferer());
        }

        $href .= '&amp;tid=' . $row['id'] . '&amp;state=' . ($row['published'] ? '' : 1);
        if (!$row['published']) {
            $icon = 'invisible.gif';
        }

        return '<a href="' . $this->addToUrl($href) . '" title="' . StringUtil::specialchars($title) . '"' . $attributes . '>' . Image::getHtml($icon, $label, 'data-state="' . ($row['published'] ? 1 : 0) . '"') . '</a> ';
    }

    /**
     * Disable/enable a user group
     *
     * @param integer $intId
     * @param boolean $blnVisible
     * @param DataContainer $dc
     */
    public function toggleVisibility($intId, $blnVisible, DataContainer $dc = null)
    {
        // Set the ID and action
        Input::setGet('id', $intId);
        Input::setGet('act', 'toggle');
        if ($dc) {
            $dc->id = $intId; // see #8043
        }

        $objVersions = new Versions('tl_mailjet_recipients', $intId);
        $objVersions->initialize();
        // Trigger the save_callback
        if (is_array($GLOBALS['TL_DCA']['tl_mailjet_recipients']['fields']['published']['save_callback'])) {
            foreach ($GLOBALS['TL_DCA']['tl_mailjet_recipients']['fields']['published']['save_callback'] as $callback) {
                if (is_array($callback)) {
                    $this->import($callback[0]);
                    $blnVisible = $this->{$callback[0]}->{$callback[1]}($blnVisible, ($dc ?: $this));
                } elseif (is_callable($callback)) {
                    $blnVisible = $callback($blnVisible, ($dc ?: $this));
                }
            }
        }
        // Update the database
        $this->Database->prepare("UPDATE tl_mailjet_recipients SET tstamp=" . time() . ", published='" . ($blnVisible ? '1' : '') . "' WHERE id=?")
            ->execute($intId);
        $objVersions->create();
    }
}
