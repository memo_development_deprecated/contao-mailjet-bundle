<?php
/**
 * @package   MailJetBundle
 * @author    Rory Zünd, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

use Contao\FormFieldModel;
use Contao\FormModel;
use Contao\Backend;
use Contao\DataContainer;
use Memo\MailJetBundle\Model\MailJetTransactionalTemplatesModel;
use NotificationCenter\Model\Language;
use NotificationCenter\Model\Message;
use NotificationCenter\Model\Notification;

$GLOBALS['TL_DCA']['tl_form']['config']['onsubmit_callback'][] = array('tl_form_custom', 'onsubmit_callback');

class tl_form_custom extends Backend
{
    public function onsubmit_callback(DataContainer $dc)
    {
        // Check if notification bundle is installed
        if (!class_exists('NotificationCenter\Model\Notification')) {
            return;
        }

        $currentId = $dc->activeRecord->id;

        $currentObject = $dc->activeRecord->object;
        $objForm = FormModel::findBy('id', $currentId);

        // Is it a Mailjet-Form?
        if ($objForm->nc_notification > 0) {

            $colMessages = Message::findByPid($objForm->nc_notification);
            if (!$colMessages) {
                return;
            }

            foreach ($colMessages as $objMessage) {

                if ($colMessageLanguages = Language::findByPid($objMessage->id)) {

                    if ($colMessageLanguages->mailjet_template > 0) {


                        $objTemplate = MailJetTransactionalTemplatesModel::findByPk($colMessageLanguages->mailjet_template);

                        // Are there variables in the template?
                        if ($objTemplate && $objTemplate->variables != '') {
                            $colFormFields = FormFieldModel::findBy('pid', $currentId);
                            $arrFieldnames = array();
                            $bolAllVariablesOK = true;
                            $bolTemplateOK = true;
                            $strBadVariables = "Das in der Benachrichtigung definierte Mailjet-Template benötigt noch weitere Formularfelder: <b>";
                            $strBadTemplate = "Im Mailjet-Template gibt es falsche Variablen, bitte ergänzen Sie folgende Variablen mit einem 'form_' davor: <b>";

                            foreach ($colFormFields as $objFormfield) {
                                if ($objFormfield->name != '') {
                                    $arrFieldnames[] = 'form_' . $objFormfield->name;
                                }

                            }

                            $arrVariables = explode(',', $objTemplate->variables);
                            foreach ($arrVariables as $strVariablename) {
                                if (!in_array($strVariablename, $arrFieldnames)) {

                                    if (stristr($strVariablename, 'form_')) {

                                        $bolAllVariablesOK = false;
                                        $strBadVariables .= $strVariablename . ", ";

                                    } else {
                                        $bolTemplateOK = false;
                                        $strBadTemplate .= $strVariablename . ", ";
                                    }

                                }
                            }

                            if (!$bolAllVariablesOK) {
                                $strBadVariables = substr($strBadVariables, 0, -2);
                                \Contao\Message::addError($strBadVariables . "</b>");
                            }

                            if (!$bolTemplateOK) {
                                $strBadTemplate = substr($strBadTemplate, 0, -2);
                                \Contao\Message::addError($strBadTemplate . "</b>");
                            }


                            if ($bolAllVariablesOK && $bolTemplateOK) {
                                $strVariables = str_replace(',', ', ', $objTemplate->variables);
                                $strVariables = str_replace('form_', '', $strVariables);
                                \Contao\Message::addConfirmation("Alle Mailjet Template-Variablen ($strVariables) kommen als Formular-Felder vor.");
                            }
                        }
                    }
                }
            }

        }
    }
}
