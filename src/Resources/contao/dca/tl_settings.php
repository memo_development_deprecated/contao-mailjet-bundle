<?php
/**
 * @package   MailJetBundle
 * @author    Rory Zünd, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

use Contao\CoreBundle\DataContainer\PaletteManipulator;

PaletteManipulator::create()
    ->addLegend('mailjet_legend', 'tl_settings', PaletteManipulator::POSITION_AFTER)
    ->addField('mailjet_dns_check', 'memo_legend', PaletteManipulator::POSITION_APPEND)
    ->applyToPalette('default', 'tl_settings');


$GLOBALS['TL_DCA']['tl_settings']['fields']['mailjet_dns_check'] = array(
    'label' => &$GLOBALS['TL_LANG']['tl_settings']['fields']['mailjet_dns_check'],
    'exclude' => true,
    'inputType' => 'checkbox',
    'eval' => array('tl_class' => 'clr', 'mandatory' => false),
);
