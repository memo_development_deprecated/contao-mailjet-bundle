<?php
/**
 * @package   MailJetBundle
 * @author    Rory Zünd, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

use Contao\Input;
use Contao\Image;
use Contao\Backend;
use Contao\DataContainer;
use Contao\StringUtil;
use Contao\Versions;
use Contao\System;

/**
 * Table tl_mailjet_integration
 */
$GLOBALS['TL_DCA']['tl_mailjet_integration'] = array
(

    // Config
    'config' => array
    (
        'dataContainer' => Contao\DC_Table::class,
        'enableVersioning' => true,
        'sql' => array
        (
            'keys' => array
            (
                'id' => 'primary'
            )
        )
    ),

    // List
    'list' => array
    (
        'sorting' => array
        (
            'mode' => 2,
            'fields' => array('title'),
            'flag' => 1,
            'panelLayout' => 'search, filter; sort, limit'
        ),
        'label' => array
        (
            'fields' => array('title'),
        ),
        'global_operations' => array
        (
            'all' => array
            (
                'label' => &$GLOBALS['TL_LANG']['MSC']['all'],
                'href' => 'act=select',
                'class' => 'header_edit_all',
                'attributes' => 'onclick="Backend.getScrollOffset();" accesskey="e"'
            )
        ),
        'operations' => array
        (
            'edit' => array
            (
                'label' => &$GLOBALS['TL_LANG']['tl_mailjet_integration']['edit'],
                'href' => 'act=edit',
                'icon' => 'edit.gif'
            ),
            'copy' => array
            (
                'label' => &$GLOBALS['TL_LANG']['tl_mailjet_integration']['copy'],
                'href' => 'act=copy',
                'icon' => 'copy.gif'
            ),
            'delete' => array
            (
                'label' => &$GLOBALS['TL_LANG']['tl_mailjet_integration']['delete'],
                'href' => 'act=delete',
                'icon' => 'delete.gif',
                'attributes' => 'onclick="if(!confirm(\'' . ($GLOBALS['TL_LANG']['MSC']['deleteConfirm'] ?? null) . '\'))return false;Backend.getScrollOffset()"'
            ),
            'toggle' => array
            (
                'label' => &$GLOBALS['TL_LANG']['tl_mailjet_integration']['toggle'],
                'icon' => 'visible.gif',
                'attributes' => 'onclick="Backend.getScrollOffset();return AjaxRequest.toggleVisibility(this,%s)"',
                'button_callback' => array('tl_mailjet_integration_memo', 'toggleIcon')
            ),
            'show' => array
            (
                'label' => &$GLOBALS['TL_LANG']['tl_mailjet_integration']['show'],
                'href' => 'act=show',
                'icon' => 'show.gif'
            ),
        )
    ),

    // Select
    'select' => array
    (
        'buttons_callback' => array()
    ),

    // Edit
    'edit' => array
    (
        'buttons_callback' => array()
    ),

    // Palettes
    'palettes' => array
    (
        '__selector__' => ['type', 'contactlisttype'],
        'default' => '{general_legend}, title, api_id, type;{expert_legend}, published;',
        'with_optin' => '{general_legend}, title, api_id, type;{registration_legend}, form_id;{optinmail_legend}, mail_template_id, sender_email, sender_name, subject;{optin_legend}, opt_in_page; {result_legend}, contactlisttype ;{expert_legend}, published;',
        'with_optin_secure' => '{general_legend}, title, api_id, type;{registration_legend}, form_id;{optinmail_legend}, mail_template_id, sender_email, sender_name, subject;{optin_legend}, opt_in_page; {result_legend}, contactlisttype ;{expert_legend}, published;',
        'without_optin' => '{general_legend}, title, api_id, type;{registration_legend}, form_id;{result_legend}, contactlisttype;{expert_legend}, published;',
    ),

    // Subpalettes
    'subpalettes' => array
    (
        'contactlisttype_preset' => 'contactlist_id',
    ),

    // Fields
    'fields' => array
    (
        'id' => array
        (
            'sql' => "int(10) unsigned NOT NULL auto_increment"
        ),
        'tstamp' => array
        (
            'sql' => "int(10) unsigned NOT NULL default '0'"
        ),
        'title' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_integration']['title'],
            'exclude' => true,
            'filter' => false,
            'search' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => array('mandatory' => true, 'maxlength' => 255, 'tl_class' => 'w50'),
            'sql' => "varchar(255) NOT NULL default ''",
        ),
        'api_id' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_integration']['api_id'],
            'exclude' => true,
            'filter' => true,
            'search' => false,
            'sorting' => false,
            'inputType' => 'select',
            'foreignKey' => 'tl_mailjet_api.title',
            'relation' => array('type' => 'hasOne', 'load' => 'lazy'),
            'eval' => array('mandatory' => true, 'includeBlankOption' => true, 'tl_class' => 'w50'),
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ),
        'type' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_integration']['type'],
            'exclude' => true,
            'filter' => true,
            'search' => false,
            'sorting' => false,
            'inputType' => 'select',
            'explanation' => 'mailjet_integration_type',
            'options' => array('with_optin' => 'Anmeldung mit Double-Opt-In', 'with_optin_secure'=> 'Anmeldung mit Triple-Opt-In' ,'without_optin' => 'Anmeldung Direkt / ohne Opt-In'),
            'eval' => array('mandatory' => true, 'tl_class' => 'w50 clr', 'submitOnChange' => true, 'helpwizard' => true),
            'sql' => "varchar(20) NOT NULL default 'with_optin'",
        ),
        'form_id' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_integration']['form_id'],
            'exclude' => true,
            'filter' => true,
            'search' => false,
            'sorting' => false,
            'inputType' => 'select',
            'foreignKey' => 'tl_form.title',
            'relation' => array('type' => 'hasOne', 'load' => 'lazy'),
            'eval' => array('mandatory' => true, 'includeBlankOption' => true, 'tl_class' => 'w50 clr'),
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ),
        'mail_template_id' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_integration']['mail_template_id'],
            'exclude' => true,
            'filter' => false,
            'search' => true,
            'sorting' => true,
            'inputType' => 'select',
            'foreignKey' => 'tl_mailjet_transactional_templates.title',
            'relation' => array('type' => 'hasOne', 'load' => 'lazy'),
            'eval' => array('mandatory' => true, 'includeBlankOption' => true, 'tl_class' => 'w50'),
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ),
        'sender_email' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_integration']['sender_email'],
            'exclude' => true,
            'filter' => false,
            'search' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => array('mandatory' => true, 'maxlength' => 255, 'tl_class' => 'w50 clr', 'rgxp' => 'email'),
            'sql' => "varchar(255) NOT NULL default ''",
        ),
        'sender_name' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_integration']['sender_name'],
            'exclude' => true,
            'filter' => false,
            'search' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => array('mandatory' => true, 'maxlength' => 255, 'tl_class' => 'w50'),
            'sql' => "varchar(255) NOT NULL default ''",
        ),
        'subject' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_integration']['subject'],
            'exclude' => true,
            'filter' => false,
            'search' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => array('mandatory' => true, 'maxlength' => 255, 'tl_class' => 'w50'),
            'sql' => "varchar(255) NOT NULL default ''",
        ),
        'opt_in_page' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_integration']['opt_in_page'],
            'exclude' => true,
            'filter' => false,
            'search' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => array('mandatory' => true, 'maxlength' => 255, 'tl_class' => 'w50 clr', 'dcaPicker' => true, 'rgxp' => 'url', 'decodeEntities' => true),
            'sql' => "varchar(255) NOT NULL default ''",
        ),
        'contactlisttype' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_integration']['contactlisttype'],
            'default' => 'preset',
            'exclude' => true,
            'filter' => true,
            'search' => false,
            'sorting' => false,
            'inputType' => 'select',
            'options' => array('preset' => 'Vordefiniert (durch Admin)', 'select' => 'Auswahl im Formular (durch Endnutzer)'),
            'eval' => array('mandatory' => true, 'tl_class' => 'w50 clr', 'submitOnChange' => true),
            'sql' => "varchar(15) NOT NULL default 'preset'",
        ),
        'contactlist_id' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_integration']['contactlist_id'],
            'exclude' => true,
            'filter' => false,
            'search' => true,
            'sorting' => true,
            'inputType' => 'select',
            'foreignKey' => 'tl_mailjet_contactlists.title',
            'relation' => array('type' => 'hasOne', 'load' => 'lazy'),
            'eval' => array('mandatory' => true, 'includeBlankOption' => true, 'tl_class' => 'w50'),
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ),
        'published' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_integration']['published'],
            'exclude' => true,
            'filter' => true,
            'search' => false,
            'sorting' => false,
            'inputType' => 'checkbox',
            'eval' => array('doNotCopy' => true, 'tl_class' => 'w50 clr'),
            'sql' => "char(1) NOT NULL default ''",
        ),
    )
);


/**
 * Class tl_mailjet_integration_memo
 * Definition der Callback-Funktionen für das Datengefäss.
 */
class tl_mailjet_integration_memo extends Backend
{

    /**
     * Return the "toggle visibility" button
     *
     * @param array $row
     * @param string $href
     * @param string $label
     * @param string $title
     * @param string $icon
     * @param string $attributes
     *
     * @return string
     */
    public function toggleIcon($row, $href, $label, $title, $icon, $attributes)
    {
        if (strlen(Input::get('tid'))) {
            $this->toggleVisibility(Input::get('tid'), (Input::get('state') == 1), (@func_get_arg(12) ?: null));
            $this->redirect($this->getReferer());
        }

        $href .= '&amp;tid=' . $row['id'] . '&amp;state=' . ($row['published'] ? '' : 1);
        if (!$row['published']) {
            $icon = 'invisible.gif';
        }

        return '<a href="' . $this->addToUrl($href) . '" title="' . StringUtil::specialchars($title) . '"' . $attributes . '>' . Image::getHtml($icon, $label, 'data-state="' . ($row['published'] ? 1 : 0) . '"') . '</a> ';
    }

    /**
     * Disable/enable a user group
     *
     * @param integer $intId
     * @param boolean $blnVisible
     * @param DataContainer $dc
     */
    public function toggleVisibility($intId, $blnVisible, DataContainer $dc = null)
    {
        // Set the ID and action
        Input::setGet('id', $intId);
        Input::setGet('act', 'toggle');
        if ($dc) {
            $dc->id = $intId; // see #8043
        }

        $objVersions = new Versions('tl_mailjet_integration', $intId);
        $objVersions->initialize();
        // Trigger the save_callback
        if (is_array($GLOBALS['TL_DCA']['tl_mailjet_integration']['fields']['published']['save_callback'])) {
            foreach ($GLOBALS['TL_DCA']['tl_mailjet_integration']['fields']['published']['save_callback'] as $callback) {
                if (is_array($callback)) {
                    $this->import($callback[0]);
                    $blnVisible = $this->{$callback[0]}->{$callback[1]}($blnVisible, ($dc ?: $this));
                } elseif (is_callable($callback)) {
                    $blnVisible = $callback($blnVisible, ($dc ?: $this));
                }
            }
        }
        // Update the database
        $this->Database->prepare("UPDATE tl_mailjet_integration SET tstamp=" . time() . ", published='" . ($blnVisible ? '1' : '') . "' WHERE id=?")
            ->execute($intId);
        $objVersions->create();
    }
}
