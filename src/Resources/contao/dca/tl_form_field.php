<?php

/*
 * Extend palettes
 */

$GLOBALS['TL_DCA']['tl_form_field']['palettes']['mailjet_contactlists'] = '{type_legend},type;{expert_legend:hide},class;{template_legend:hide},customTpl;{invisible_legend:hide},invisible';

foreach ($GLOBALS['TL_DCA']['tl_form_field']['palettes'] as $key => $value) {
    if (!in_array($key, array('__selector__', 'submit', 'captcha', 'upload', 'fieldsetStart', 'fieldsetStop', 'mailjet_contactlists', 'explanation'))) {
        $GLOBALS['TL_DCA']['tl_form_field']['palettes'][$key] .= ';{mailjet_legend},mailjetfield';
    }
}

if (is_array($GLOBALS['TL_DCA']['tl_form_field']['palettes']['__selector__'])) {
    $GLOBALS['TL_DCA']['tl_form_field']['palettes']['__selector__'][] = 'mailjetfield';
} else {
    $GLOBALS['TL_DCA']['tl_form_field']['palettes']['__selector__'] = array('mailjetfield');
}

$GLOBALS['TL_DCA']['tl_form_field']['subpalettes']['mailjetfield'] = 'mailjetfieldname';

/*
 * Fields
 */
$GLOBALS['TL_DCA']['tl_form_field']['fields']['mailjetfield'] = [
    'label' => &$GLOBALS['TL_LANG']['tl_form_field']['mailjetfield'],
    'exclude' => true,
    'inputType' => 'checkbox',
    'eval' => array('tl_class' => 'm12 long clr', 'submitOnChange' => true, 'doNotCopy' => true),
    'sql' => "char(1) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_form_field']['fields']['mailjetfieldname'] = [
    'label' => &$GLOBALS['TL_LANG']['tl_form_field']['mailjetfieldname'],
    'exclude' => true,
    'inputType' => 'text',
    'eval' => array('tl_class' => 'w50 clr', 'doNotCopy' => true),
    'sql' => "varchar(255) NOT NULL default ''"
];
