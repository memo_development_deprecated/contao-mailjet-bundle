<?php
/**
 * @package   MailJetBundle
 * @author    Rory Zünd, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

$GLOBALS['TL_DCA']['tl_nc_gateway']['fields']['mailjet_api'] = array(
    'label' => &$GLOBALS['TL_LANG']['tl_nc_gateway']['mailjet_api'],
    'exclude' => true,
    'inputType' => 'select',
    'foreignKey' => 'tl_mailjet_api.title',
    'relation' => array('type' => 'hasOne', 'load' => 'lazy'),
    'eval' => array('mandatory' => true, 'includeBlankOption' => true, 'tl_class' => 'w50'),
    'sql' => "int(10) unsigned NOT NULL default '0'",
);

$GLOBALS['TL_DCA']['tl_nc_gateway']['palettes']['mailjet'] = '{title_legend},title,type;{gateway_legend},mailjet_api';
