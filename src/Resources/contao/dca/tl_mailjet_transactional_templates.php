<?php
/**
 * @package   MailJetBundle
 * @author    Rory Zünd, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

use Contao\Backend;
use Contao\DataContainer;
use Contao\Image;
use Contao\Input;
use Contao\StringUtil;
use Contao\Versions;

/**
 * Table tl_mailjet_transactional_templates
 */
$GLOBALS['TL_DCA']['tl_mailjet_transactional_templates'] = array
(

    // Config
    'config' => array
    (
        'dataContainer' => Contao\DC_Table::class,
        'enableVersioning' => true,
        'sql' => array
        (
            'keys' => array
            (
                'id' => 'primary'
            )
        )
    ),

    // List
    'list' => array
    (
        'sorting' => array
        (
            'mode' => 2,
            'fields' => array('api_id', 'title'),
            'flag' => 1,
            'panelLayout' => 'search, filter; sort, limit'
        ),
        'label' => array
        (
            'fields' => array('title', 'template_id'),
            'format' => '%s <span style="color:#999;padding-left:3px;">[%s]</span>'
        ),
        'global_operations' => array
        (
            'import' => array
            (
                'label' => &$GLOBALS['TL_LANG']['MSC']['sync'],
                'href' => 'key=sync',
                'class' => 'sync',
                'attributes' => 'onclick="Backend.getScrollOffset();" accesskey="e"'
            )
        ),
        'operations' => array
        (
            'edit' => array
            (
                'label' => &$GLOBALS['TL_LANG']['tl_mailjet_transactional_templates']['edit'],
                'href' => 'act=edit',
                'icon' => 'edit.gif'
            ),
            'toggle' => array
            (
                'label' => &$GLOBALS['TL_LANG']['tl_mailjet_transactional_templates']['toggle'],
                'icon' => 'visible.gif',
                'attributes' => 'onclick="Backend.getScrollOffset();return AjaxRequest.toggleVisibility(this,%s)"',
                'button_callback' => array('tl_mailjet_transactional_templates_memo', 'toggleIcon')
            ),
            'show' => array
            (
                'label' => &$GLOBALS['TL_LANG']['tl_mailjet_transactional_templates']['show'],
                'href' => 'act=show',
                'icon' => 'show.gif'
            ),
        )
    ),

    // Select
    'select' => array
    (
        'buttons_callback' => array()
    ),

    // Edit
    'edit' => array
    (
        'buttons_callback' => array()
    ),

    // Palettes
    'palettes' => array
    (
        'default' => '{general_legend}, orig_title, title, api_id, template_id , sender_email, sender_name, subject, variables, template_code, published;',
    ),

    // Subpalettes
    'subpalettes' => array
    (
        '' => '',
    ),

    // Fields
    'fields' => array
    (
        'id' => array
        (
            'sql' => "int(10) unsigned NOT NULL auto_increment"
        ),
        'tstamp' => array
        (
            'sql' => "int(10) unsigned NOT NULL default '0'"
        ),
        'title' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_transactional_templates']['title'],
            'exclude' => true,
            'filter' => false,
            'search' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => array('mandatory' => true, 'maxlength' => 255, 'tl_class' => 'w50'),
            'sql' => "varchar(255) NOT NULL default ''",
        ),
        'api_id' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_transactional_templates']['api_id'],
            'exclude' => true,
            'filter' => true,
            'search' => false,
            'sorting' => false,
            'inputType' => 'select',
            'foreignKey' => 'tl_mailjet_api.title',
            'relation' => array('type' => 'hasOne', 'load' => 'lazy'),
            'eval' => array('mandatory' => true, 'includeBlankOption' => true, 'tl_class' => 'clr w50', 'readonly' => true),
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ),
        'orig_title' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_transactional_templates']['orig_title'],
            'exclude' => true,
            'filter' => false,
            'search' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => array('mandatory' => true, 'maxlength' => 255, 'tl_class' => 'w50', 'readonly' => true),
            'sql' => "varchar(255) NOT NULL default ''",
        ),
        'template_id' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_transactional_templates']['template_id'],
            'exclude' => true,
            'filter' => false,
            'search' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => array('mandatory' => true, 'maxlength' => 255, 'tl_class' => 'w50', 'readonly' => true),
            'sql' => "varchar(255) NOT NULL default ''",
        ),
        'template_code' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_transactional_templates']['template_code'],
            'exclude' => true,
            'filter' => false,
            'search' => true,
            'sorting' => true,
            'inputType' => 'textarea',
            'eval' => array('mandatory' => true, 'tl_class' => 'clr long', 'readonly' => true, 'allowHtml' => true),
            'sql' => "LONGTEXT NULL",
        ),
        'sender_email' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_transactional_templates']['sender_email'],
            'exclude' => true,
            'filter' => false,
            'search' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => array('mandatory' => true, 'maxlength' => 255, 'tl_class' => 'w50', 'readonly' => true),
            'sql' => "varchar(255) NULL",
        ),
        'sender_name' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_transactional_templates']['sender_name'],
            'exclude' => true,
            'filter' => false,
            'search' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => array('mandatory' => true, 'maxlength' => 255, 'tl_class' => 'w50', 'readonly' => true),
            'sql' => "varchar(255) NULL",
        ),
        'subject' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_transactional_templates']['subject'],
            'exclude' => true,
            'filter' => false,
            'search' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => array('mandatory' => true, 'maxlength' => 255, 'tl_class' => 'long clr', 'readonly' => true),
            'sql' => "varchar(255) NULL",
        ),
        'variables' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_transactional_templates']['variables'],
            'exclude' => true,
            'filter' => false,
            'search' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => array('mandatory' => true, 'tl_class' => 'long clr', 'readonly' => true),
            'sql' => "TEXT NULL",
        ),
        'published' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_transactional_templates']['published'],
            'exclude' => true,
            'filter' => true,
            'search' => false,
            'sorting' => false,
            'inputType' => 'checkbox',
            'eval' => array('doNotCopy' => true, 'tl_class' => 'w50 clr'),
            'sql' => "char(1) NOT NULL default ''",
        ),
    )
);


/**
 * Class tl_mailjet_transactional_templates_memo
 * Definition der Callback-Funktionen für das Datengefäss.
 */
class tl_mailjet_transactional_templates_memo extends Backend
{

    /**
     * Return the "toggle visibility" button
     *
     * @param array $row
     * @param string $href
     * @param string $label
     * @param string $title
     * @param string $icon
     * @param string $attributes
     *
     * @return string
     */
    public function toggleIcon($row, $href, $label, $title, $icon, $attributes)
    {
        if (strlen(Input::get('tid'))) {
            $this->toggleVisibility(Input::get('tid'), (Input::get('state') == 1), (@func_get_arg(12) ?: null));
            $this->redirect($this->getReferer());
        }

        $href .= '&amp;tid=' . $row['id'] . '&amp;state=' . ($row['published'] ? '' : 1);
        if (!$row['published']) {
            $icon = 'invisible.gif';
        }

        return '<a href="' . $this->addToUrl($href) . '" title="' . StringUtil::specialchars($title) . '"' . $attributes . '>' . Image::getHtml($icon, $label, 'data-state="' . ($row['published'] ? 1 : 0) . '"') . '</a> ';
    }

    /**
     * Disable/enable a user group
     *
     * @param integer $intId
     * @param boolean $blnVisible
     * @param DataContainer $dc
     */
    public function toggleVisibility($intId, $blnVisible, DataContainer $dc = null)
    {
        // Set the ID and action
        Input::setGet('id', $intId);
        Input::setGet('act', 'toggle');
        if ($dc) {
            $dc->id = $intId; // see #8043
        }

        $objVersions = new Versions('tl_mailjet_transactional_templates', $intId);
        $objVersions->initialize();
        // Trigger the save_callback
        if (is_array($GLOBALS['TL_DCA']['tl_mailjet_transactional_templates']['fields']['published']['save_callback'])) {
            foreach ($GLOBALS['TL_DCA']['tl_mailjet_transactional_templates']['fields']['published']['save_callback'] as $callback) {
                if (is_array($callback)) {
                    $this->import($callback[0]);
                    $blnVisible = $this->{$callback[0]}->{$callback[1]}($blnVisible, ($dc ?: $this));
                } elseif (is_callable($callback)) {
                    $blnVisible = $callback($blnVisible, ($dc ?: $this));
                }
            }
        }
        // Update the database
        $this->Database->prepare("UPDATE tl_mailjet_transactional_templates SET tstamp=" . time() . ", published='" . ($blnVisible ? '1' : '') . "' WHERE id=?")
            ->execute($intId);
        $objVersions->create();
    }
}
