<?php
/**
 * @package   MailJetBundle
 * @author    Rory Zünd, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

use Contao\Input;
use Contao\Image;
use Contao\Backend;
use Contao\DataContainer;
use Contao\StringUtil;
use Contao\Versions;
use Contao\System;

/**
 * Table tl_mailjet_massmailing
 */
$GLOBALS['TL_DCA']['tl_mailjet_massmailing'] = array
(

    // Config
    'config' => array
    (
        'dataContainer' => Contao\DC_Table::class,
        'enableVersioning' => true,
        'sql' => array
        (
            'keys' => array
            (
                'id' => 'primary'
            )
        )
    ),

    // List
    'list' => array
    (
        'sorting' => array
        (
            'mode' => 2,
            'fields' => array('title'),
            'flag' => 1,
            'panelLayout' => 'search, filter; sort, limit'
        ),
        'label' => array
        (
            'fields' => array('title'),
        ),
        'global_operations' => array
        (
            'all' => array
            (
                'label' => &$GLOBALS['TL_LANG']['MSC']['all'],
                'href' => 'act=select',
                'class' => 'header_edit_all',
                'attributes' => 'onclick="Backend.getScrollOffset();" accesskey="e"'
            )
        ),
        'operations' => array
        (
            'edit' => array
            (
                'label' => &$GLOBALS['TL_LANG']['tl_mailjet_massmailing']['edit'],
                'href' => 'act=edit',
                'icon' => 'edit.gif'
            ),
            'copy' => array
            (
                'label' => &$GLOBALS['TL_LANG']['tl_mailjet_massmailing']['copy'],
                'href' => 'act=copy',
                'icon' => 'copy.gif'
            ),
            'delete' => array
            (
                'label' => &$GLOBALS['TL_LANG']['tl_mailjet_massmailing']['delete'],
                'href' => 'act=delete',
                'icon' => 'delete.gif',
                'attributes' => 'onclick="if(!confirm(\'' . ($GLOBALS['TL_LANG']['MSC']['deleteConfirm'] ?? null) . '\'))return false;Backend.getScrollOffset()"'
            ),
            'send' => array
            (
                'label' => &$GLOBALS['TL_LANG']['tl_mailjet_massmailing']['send'],
                'href' => 'key=send',
                'icon' => 'manager.svg',
                'attributes' => 'onclick="Backend.getScrollOffset();"',
                'button_callback' => array('tl_mailjet_massmailing', 'send_callback')
            ),
        )
    ),

    // Select
    'select' => array
    (
        'buttons_callback' => array()
    ),

    // Edit
    'edit' => array
    (
        'buttons_callback' => array()
    ),

    // Palettes
    'palettes' => array
    (
        'default' => '{general_legend}, title, api_id;{recipient_legend},recipient_archiv_id;{mail_legend}, mail_template_id, sender_email, sender_name, subject;{opt_out_legend},opt_in_page, integration_id,opt_out_page;{expert_legend}, sent, tstamp_sent;',
    ),

    // Subpalettes
    'subpalettes' => array
    (
        '' => '',
    ),

    // Fields
    'fields' => array
    (
        'id' => array
        (
            'sql' => "int(10) unsigned NOT NULL auto_increment"
        ),
        'tstamp' => array
        (
            'default' => time(),
            'sql' => "int(10) unsigned NOT NULL DEFAULT CURRENT_TIMESTAMP"
        ),
        'tstamp_sent' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_massmailing']['tstamp_sent'],
            'exclude' => true,
            'filter' => true,
            'sorting' => true,
            'flag' => 8,
            'inputType' => 'text',
            'eval' => array('rgxp' => 'datim', 'mandatory' => false, 'doNotCopy' => true, 'tl_class' => 'w50 wizard', 'readonly' => true),
            'load_callback' => array
            (
                array('tl_mailjet_massmailing', 'loadDate')
            ),
            'sql' => "int(10) NULL"
        ),
        'title' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_massmailing']['title'],
            'exclude' => true,
            'filter' => false,
            'search' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => array('mandatory' => true, 'maxlength' => 255, 'tl_class' => 'w50'),
            'sql' => "varchar(255) NOT NULL default ''",
        ),
        'api_id' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_massmailing']['api_id'],
            'exclude' => true,
            'filter' => true,
            'search' => false,
            'sorting' => false,
            'inputType' => 'select',
            'foreignKey' => 'tl_mailjet_api.title',
            'relation' => array('type' => 'hasOne', 'load' => 'lazy'),
            'eval' => array('mandatory' => true, 'includeBlankOption' => true, 'tl_class' => 'w50'),
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ),
        'recipient_archiv_id' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_massmailing']['recipient_archiv_id'],
            'exclude' => true,
            'filter' => true,
            'search' => false,
            'sorting' => false,
            'inputType' => 'select',
            'foreignKey' => 'tl_mailjet_recipients_archiv.title',
            'relation' => array('type' => 'hasOne', 'load' => 'lazy'),
            'eval' => array('mandatory' => true, 'includeBlankOption' => true, 'tl_class' => 'w50'),
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ),
        'mail_template_id' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_integration']['mail_template_id'],
            'exclude' => true,
            'filter' => false,
            'search' => true,
            'sorting' => true,
            'inputType' => 'select',
            'foreignKey' => 'tl_mailjet_transactional_templates.title',
            'relation' => array('type' => 'hasOne', 'load' => 'lazy'),
            'eval' => array('mandatory' => true, 'includeBlankOption' => true, 'tl_class' => 'w50'),
            'sql' => "int(10) NOT NULL default '0'",
        ),
        'sender_email' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_massmailing']['sender_email'],
            'exclude' => true,
            'filter' => false,
            'search' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => array('mandatory' => true, 'maxlength' => 255, 'tl_class' => 'w50 clr', 'rgxp' => 'email'),
            'sql' => "varchar(255) NOT NULL default ''",
        ),
        'sender_name' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_massmailing']['sender_name'],
            'exclude' => true,
            'filter' => false,
            'search' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => array('mandatory' => true, 'maxlength' => 255, 'tl_class' => 'w50'),
            'sql' => "varchar(255) NOT NULL default ''",
        ),
        'subject' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_massmailing']['subject'],
            'exclude' => true,
            'filter' => false,
            'search' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => array('mandatory' => true, 'maxlength' => 255, 'tl_class' => 'w50'),
            'sql' => "varchar(255) NOT NULL default ''",
        ),
        'opt_in_page' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_massmailing']['opt_in_page'],
            'exclude' => true,
            'filter' => false,
            'search' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => array('mandatory' => false, 'maxlength' => 255, 'tl_class' => 'w50 clr', 'dcaPicker' => true, 'rgxp' => 'url', 'decodeEntities' => true),
            'sql' => "varchar(255) NOT NULL default ''",
        ),
        'opt_out_page' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_massmailing']['opt_out_page'],
            'exclude' => true,
            'filter' => false,
            'search' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => array('mandatory' => true, 'maxlength' => 255, 'tl_class' => 'w50 clr', 'dcaPicker' => true, 'rgxp' => 'url', 'decodeEntities' => true),
            'sql' => "varchar(255) NOT NULL default ''",
        ),
        'integration_id' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_optin']['integration_id'],
            'exclude' => true,
            'filter' => true,
            'search' => false,
            'sorting' => true,
            'inputType' => 'select',
            'foreignKey' => 'tl_mailjet_integration.title',
            'relation' => array('type' => 'hasOne', 'load' => 'lazy'),
            'eval' => array('includeBlankOption' => true, 'tl_class' => 'w50', 'mandatory' => false),
            'sql' => "int(10) NOT NULL default '0'",
        ),
        'sent' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_massmailing']['sent'],
            'exclude' => true,
            'filter' => true,
            'search' => false,
            'sorting' => false,
            'inputType' => 'checkbox',
            'eval' => array('tl_class' => 'w50 clr'),
            'sql' => "char(1) NOT NULL default '0'",
        ),
        'published' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_massmailing']['published'],
            'exclude' => true,
            'filter' => true,
            'search' => false,
            'sorting' => false,
            'inputType' => 'checkbox',
            'eval' => array('doNotCopy' => true, 'tl_class' => 'w50 clr'),
            'sql' => "char(1) NOT NULL default ''",
        ),
    )
);


/**
 * Class tl_mailjet_massmailing
 * Definition der Callback-Funktionen für das Datengefäss.
 */
class tl_mailjet_massmailing extends Backend
{

    public function loadDate($value)
    {

        if ($value && $value > 0) {
            return strtotime(date('d.m.Y H:i:s', $value));
        } else {
            return "";
        }
    }

    /**
     * Return the "toggle visibility" button
     *
     * @param array $row
     * @param string $href
     * @param string $label
     * @param string $title
     * @param string $icon
     * @param string $attributes
     *
     * @return string
     */
    public function toggleIcon($row, $href, $label, $title, $icon, $attributes)
    {
        if (strlen(Input::get('tid'))) {
            $this->toggleVisibility(Input::get('tid'), (Input::get('state') == 1), (@func_get_arg(12) ?: null));
            $this->redirect($this->getReferer());
        }

        $href .= '&amp;tid=' . $row['id'] . '&amp;state=' . ($row['published'] ? '' : 1);
        if (!$row['published']) {
            $icon = 'invisible.gif';
        }

        return '<a href="' . $this->addToUrl($href) . '" title="' . StringUtil::specialchars($title) . '"' . $attributes . '>' . Image::getHtml($icon, $label, 'data-state="' . ($row['published'] ? 1 : 0) . '"') . '</a> ';
    }

    /**
     * Disable/enable a user group
     *
     * @param integer $intId
     * @param boolean $blnVisible
     * @param DataContainer $dc
     */
    public function toggleVisibility($intId, $blnVisible, DataContainer $dc = null)
    {
        // Set the ID and action
        Input::setGet('id', $intId);
        Input::setGet('act', 'toggle');
        if ($dc) {
            $dc->id = $intId; // see #8043
        }

        $objVersions = new Versions('tl_mailjet_massmailing', $intId);
        $objVersions->initialize();
        // Trigger the save_callback
        if (is_array($GLOBALS['TL_DCA']['tl_mailjet_massmailing']['fields']['published']['save_callback'])) {
            foreach ($GLOBALS['TL_DCA']['tl_mailjet_massmailing']['fields']['published']['save_callback'] as $callback) {
                if (is_array($callback)) {
                    $this->import($callback[0]);
                    $blnVisible = $this->{$callback[0]}->{$callback[1]}($blnVisible, ($dc ?: $this));
                } elseif (is_callable($callback)) {
                    $blnVisible = $callback($blnVisible, ($dc ?: $this));
                }
            }
        }
        // Update the database
        $this->Database->prepare("UPDATE tl_mailjet_massmailing SET tstamp=" . time() . ", published='" . ($blnVisible ? '1' : '') . "' WHERE id=?")
            ->execute($intId);
        $objVersions->create();
    }

    public function send_callback($arrRow, $href, $label, $title, $icon, $attributes, $strTable, $arrRootIds, $arrChildRecordIds, $blnCircularReference, $strPrevious, $strNext)
    {
        $objMailing = Memo\MailJetBundle\Model\MailJetMassmailingModel::findOneById($arrRow['id']);

        if (!$objMailing->sent) {
            return '<a href="' . $this->addToUrl($href . '&id=' . $arrRow['id']) . '" title="' . StringUtil::specialchars($title) . '"' . $attributes . '>' . Image::getHtml($icon, $label) . '</a> ';
        }
        return '';
    }
}
