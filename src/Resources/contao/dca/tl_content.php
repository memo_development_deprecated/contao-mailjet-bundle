<?php
/**
 * @package   MailJetBundle
 * @author    Rory Zünd, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */


/**
 * Add palettes
 */

use Contao\Backend;

$GLOBALS['TL_DCA']['tl_content']['palettes']['mailjetregistration'] = '{type_legend},type,headline;{source_legend},mailjet_integration_id;{protected_legend:hide},protected;{expert_legend:hide},guests,cssID;{invisible_legend:hide},invisible,start,stop';

$GLOBALS['TL_DCA']['tl_content']['palettes']['mailjetoptin'] = '{type_legend},type, mailjet_optin_title, mailjet_optin_text, mailjet_optin_button_text;{protected_legend:hide},protected;{expert_legend:hide},guests,cssID;{invisible_legend:hide},invisible,start,stop';

$GLOBALS['TL_DCA']['tl_content']['palettes']['mailjetmassmailoptout'] = '{type_legend},type,headline;{protected_legend:hide},protected;{expert_legend:hide},guests,cssID;{invisible_legend:hide},invisible,start,stop';

/**
 * Add fields
 */

$GLOBALS['TL_DCA']['tl_content']['fields']['mailjet_integration_id'] = [
    'label' => &$GLOBALS['TL_LANG']['tl_content']['mailjet_integration_id'],
    'exclude' => true,
    'inputType' => 'select',
    'options_callback' => array('tl_mailjet_api', 'getIntegrations'),
    'eval' => array('mandatory' => true, 'multiple' => false, 'tl_class' => 'w50 clr', 'includeBlankOption' => true),
    'sql' => "int(11) NULL",
];

$GLOBALS['TL_DCA']['tl_content']['fields']['mailjet_api_id'] = [
    'label' => &$GLOBALS['TL_LANG']['tl_content']['mailjet_api_id'],
    'exclude' => true,
    'inputType' => 'select',
    'options_callback' => array('tl_mailjet_api', 'getAPIs'),
    'eval' => array('mandatory' => true, 'multiple' => false, 'tl_class' => 'w50 clr', 'includeBlankOption' => true),
    'sql' => "int(11) NULL",
];

$GLOBALS['TL_DCA']['tl_content']['fields']['mailjet_optin_title'] = [
    'label' => &$GLOBALS['TL_LANG']['tl_content']['mailjet_optin_title'],
    'exclude' => true,
    'inputType' => 'text',
    'eval' => array('mandatory' => false, 'tl_class' => 'w50 clr',),
    'sql' => "varchar(255) NOT NULL default ''",
];

$GLOBALS['TL_DCA']['tl_content']['fields']['mailjet_optin_button_text'] = [
    'label' => &$GLOBALS['TL_LANG']['tl_content']['mailjet_optin_button_text'],
    'exclude' => true,
    'inputType' => 'text',
    'eval' => array('mandatory' => false, 'tl_class' => 'w50 clr',),
    'sql' => "varchar(255) NOT NULL default ''",
];

$GLOBALS['TL_DCA']['tl_content']['fields']['mailjet_optin_text'] = [
    'label' => &$GLOBALS['TL_LANG']['tl_content']['mailjet_optin_text'],
    'exclude' => true,
    'inputType' => 'textarea',
    'eval' => array('mandatory' => true, 'tl_class' => 'long clr', 'rte' => 'tinyMCE'),
    'sql' => "TEXT NULL",
];


class tl_mailjet_api extends Backend
{

    public function getAPIs()
    {
        $this->import('Database');
        $objAPIs = $this->Database->prepare("SELECT * FROM tl_mailjet_api WHERE published='1' ORDER BY title ASC")->execute();

        $return = array();
        while ($objAPIs->next()) {
            $return[$objAPIs->id] = $objAPIs->title;
        }
        return $return;
    }

    public function getIntegrations()
    {
        $this->import('Database');
        $objForms = $this->Database->prepare("SELECT * FROM tl_mailjet_integration WHERE published='1' ORDER BY title ASC")->execute();

        $return = array();
        while ($objForms->next()) {
            $return[$objForms->id] = $objForms->title;
        }
        return $return;
    }

}
