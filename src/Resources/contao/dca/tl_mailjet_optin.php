<?php
/**
 * @package   MailJetBundle
 * @author    Rory Zünd, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

use Contao\Input;
use Contao\Image;
use Contao\Backend;
use Contao\DataContainer;
use Contao\Versions;
use Contao\System;

/**
 * Table tl_mailjet_optin
 */
$GLOBALS['TL_DCA']['tl_mailjet_optin'] = array
(

    // Config
    'config' => array
    (
        'dataContainer' => Contao\DC_Table::class,
        'enableVersioning' => true,
        'sql' => array
        (
            'keys' => array
            (
                'id' => 'primary'
            )
        )
    ),

    // List
    'list' => array
    (
        'sorting' => array
        (
            'mode' => 1,
            'fields' => array('registry_tstamp'),
            'flag' => 8,
            'panelLayout' => 'search, filter; sort, limit'
        ),
        'label' => array
        (
            'fields' => array('email'),
            'label_callback' => array('tl_mailjet_optin', 'label_callback')
        ),
        'global_operations' => array
        (
            'all' => array
            (
                'label' => &$GLOBALS['TL_LANG']['MSC']['all'],
                'href' => 'act=select',
                'class' => 'header_edit_all',
                'attributes' => 'onclick="Backend.getScrollOffset();" accesskey="e"'
            )
        ),
        'operations' => array
        (
            'edit' => array
            (
                'label' => &$GLOBALS['TL_LANG']['tl_mailjet_optin']['edit'],
                'href' => 'act=edit',
                'icon' => 'edit.gif'
            ),
            'delete' => array
            (
                'label' => &$GLOBALS['TL_LANG']['tl_mailjet_optin']['delete'],
                'href' => 'act=delete',
                'icon' => 'delete.gif',
                'attributes' => 'onclick="if(!confirm(\'' . ($GLOBALS['TL_LANG']['MSC']['deleteConfirm'] ?? null) . '\'))return false;Backend.getScrollOffset()"'
            ),
            'show' => array
            (
                'label' => &$GLOBALS['TL_LANG']['tl_mailjet_optin']['show'],
                'href' => 'act=show',
                'icon' => 'show.gif'
            ),
        )
    ),

    // Select
    'select' => array
    (
        'buttons_callback' => array()
    ),

    // Edit
    'edit' => array
    (
        'buttons_callback' => array()
    ),

    // Palettes
    'palettes' => array
    (
        'default' => '{general_legend},mailjet_integration_id, registry_tstamp, email, email_url, hash, contactlists, optin_type, sent, sent_tstamp, opened, opened_tstamp,activated, activated_tstamp, activated_ip, activated_useragent;',
    ),

    // Subpalettes
    'subpalettes' => array
    (
        '' => '',
    ),

    // Fields
    'fields' => array
    (
        'id' => array
        (
            'sql' => "int(10) unsigned NOT NULL auto_increment"
        ),
        'tstamp' => array
        (
            'sql' => "int(10) unsigned NOT NULL default '0'"
        ),
        'email' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_optin']['email'],
            'exclude' => true,
            'filter' => false,
            'search' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => array('mandatory' => true, 'maxlength' => 255, 'tl_class' => 'clr w50', 'readonly' => true),
            'sql' => "varchar(255) NOT NULL default ''",
        ),
        'email_url' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_optin']['email_url'],
            'exclude' => true,
            'filter' => false,
            'search' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => array('mandatory' => true, 'maxlength' => 255, 'tl_class' => 'w50', 'readonly' => true),
            'sql' => "varchar(255) NOT NULL default ''",
        ),
        'hash' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_optin']['hash'],
            'exclude' => true,
            'filter' => false,
            'search' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => array('mandatory' => true, 'maxlength' => 255, 'tl_class' => 'clr w50', 'readonly' => true),
            'sql' => "varchar(255) NOT NULL default ''",
        ),
        'sent' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_optin']['sent'],
            'exclude' => true,
            'filter' => true,
            'search' => false,
            'sorting' => false,
            'inputType' => 'checkbox',
            'eval' => array('doNotCopy' => true, 'tl_class' => 'w50 m12 clr', 'readonly' => true),
            'sql' => "char(1) NOT NULL default '0'",
        ),
        'opened' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_optin']['opened'],
            'exclude' => true,
            'filter' => true,
            'search' => false,
            'sorting' => false,
            'inputType' => 'checkbox',
            'eval' => array('doNotCopy' => true, 'tl_class' => 'w50 m12 clr', 'readonly' => true),
            'sql' => "char(1) NOT NULL default ''",
        ),

        'activated' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_optin']['activated'],
            'exclude' => true,
            'filter' => true,
            'search' => false,
            'sorting' => false,
            'inputType' => 'checkbox',
            'eval' => array('doNotCopy' => true, 'tl_class' => 'w50 m12 clr', 'readonly' => true),
            'sql' => "char(1) NOT NULL default ''",
        ),
        'registry_tstamp' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_optin']['registry_tstamp'],
            'default' => time(),
            'exclude' => true,
            'filter' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => array('rgxp' => 'datim', 'mandatory' => false, 'doNotCopy' => true, 'datepicker' => true, 'tl_class' => 'w50 wizard', 'readonly' => true),
            'load_callback' => array
            (
                array('tl_mailjet_optin', 'loadDate')
            ),
            'sql' => "int(10) unsigned NOT NULL default 0"
        ),
        'sent_tstamp' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_optin']['sent_tstamp'],
            'default' => 0,
            'exclude' => true,
            'filter' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => array('rgxp' => 'datim', 'mandatory' => false, 'doNotCopy' => true, 'datepicker' => true, 'tl_class' => 'w50 wizard clr', 'readonly' => true),
            'load_callback' => array
            (
                array('tl_mailjet_optin', 'loadDate')
            ),
            'sql' => "int(10) unsigned NOT NULL default 0"
        ),
        'opened_tstamp' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_optin']['opened_tstamp'],
            'default' => 0,
            'exclude' => true,
            'filter' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => array('rgxp' => 'datim', 'mandatory' => false, 'doNotCopy' => true, 'datepicker' => true, 'tl_class' => 'w50 wizard clr', 'readonly' => true),
            'load_callback' => array
            (
                array('tl_mailjet_optin', 'loadDate')
            ),
            'sql' => "int(10) unsigned NOT NULL default 0"
        ),
        'activated_tstamp' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_optin']['activated_tstamp'],
            'default' => 0,
            'exclude' => true,
            'filter' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => array('rgxp' => 'datim', 'mandatory' => false, 'doNotCopy' => true, 'datepicker' => true, 'tl_class' => 'w50 wizard clr', 'readonly' => true),
            'load_callback' => array
            (
                array('tl_mailjet_optin', 'loadDate')
            ),
            'sql' => "int(10) unsigned NOT NULL default 0"
        ),
        'activated_ip' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_optin']['activated_ip'],
            'exclude' => true,
            'filter' => false,
            'search' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => array('mandatory' => false, 'maxlength' => 255, 'tl_class' => 'w50', 'readonly' => true),
            'sql' => "varchar(255) NOT NULL default ''",
        ),
        'activated_useragent' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_optin']['activated_useragent'],
            'exclude' => true,
            'filter' => false,
            'search' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => array('mandatory' => false, 'maxlength' => 255, 'tl_class' => 'long clr', 'readonly' => true),
            'sql' => "varchar(255) NOT NULL default ''",
        ),
        'mailjet_integration_id' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_optin']['mailjet_integration_id'],
            'exclude' => true,
            'filter' => true,
            'search' => false,
            'sorting' => true,
            'inputType' => 'select',
            'foreignKey' => 'tl_mailjet_integration.title',
            'relation' => array('type' => 'hasOne', 'load' => 'lazy'),
            'eval' => array('includeBlankOption' => true, 'tl_class' => 'w50', 'mandatory' => true, 'readonly' => true),
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ),
        'formfields' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_optin']['formfields'],
            'exclude' => true,
            'filter' => false,
            'search' => true,
            'sorting' => false,
            'inputType' => 'textarea',
            'eval' => array('mandatory' => false, 'tl_class' => 'clr long', 'readonly' => true),
            'sql' => "BLOB NULL",
        ),
        'contactlists' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_optin']['contactlists'],
            'exclude' => true,
            'filter' => true,
            'search' => false,
            'sorting' => false,
            'inputType' => 'select',
            'foreignKey' => 'tl_mailjet_contactlists.title',
            'eval' => array('mandatory' => false, 'tl_class' => 'clr', 'multiple' => true, 'chosen' => true, 'readonly' => true),
            'sql' => "BLOB NULL",
        ),
        'optin_type' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_mailjet_optin']['optin_type'],
            'exclude' => true,
            'filter' => true,
            'search' => false,
            'sorting' => true,
            'inputType' => 'select',
            'options' => array('double', 'triple', 'none'),
            'reference' => &$GLOBALS['TL_LANG']['tl_mailjet_optin']['optin_types'],
            'eval' => array('tl_class' => 'w50', 'mandatory' => true, 'readonly' => true),
            'sql' => "varchar(20) NOT NULL default 'triple'",
        ),
    )
);

class tl_mailjet_optin extends Backend
{

    /**
     * Set the timestamp to 00:00:00 (see #26)
     *
     * @param integer $value
     *
     * @return integer
     */
    public function loadDate($value)
    {
        if ($value > 0) {
            return strtotime(date('Y-m-d H:i:s', $value));
        } else {
            return '';
        }
    }

    /**
     * @param $row
     * @param $label
     * @param DataContainer|null $dc
     * @param string $imageAttribute
     * @param false $blnReturnImage
     * @param false $blnProtected
     * @return string
     */
    public function label_callback($row, $label, DataContainer $dc = null, $imageAttribute = '', $blnReturnImage = false, $blnProtected = false)
    {
        $label = $row['email'];

        if ($row['activated'] == 1) {
            $label .= ' <span style="background-color: #2ba91d;padding: 3px 5px;display: inline-block;color: white;border-radius: 5px;font-size: 0.9em;">Anmeldung abgeschlossen</span>';
        } elseif ($row['opened'] == 1) {
            $label .= ' <span style="background-color:#e39d01;padding: 3px 5px;display: inline-block;color: white;border-radius: 5px;font-size: 0.9em;">Opt-In Link geöffnet</span>';
        } elseif ($row['sent'] == 1) {
            $label .= ' <span style="background-color:#979797;padding: 3px 5px;display: inline-block;color: white;border-radius: 5px;font-size: 0.9em;">Opt-In E-Mail verschickt</span>';
        } else {
            $label .= ' <span style="background-color:#c91414;padding: 3px 5px;display: inline-block;color: white;border-radius: 5px;font-size: 0.9em;">Opt-In fehlgeschlagen</span>';
        }

        return $label;

    }

}
