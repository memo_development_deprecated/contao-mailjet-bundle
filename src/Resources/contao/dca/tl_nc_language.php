<?php
/**
 * @package   MailJetBundle
 * @author    Rory Zünd, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

$GLOBALS['TL_DCA']['tl_nc_language']['fields']['mailjet_template'] = array(
    'label' => &$GLOBALS['TL_LANG']['tl_nc_language']['mailjet_template'],
    'inputType' => 'select',
    'foreignKey' => 'tl_mailjet_transactional_templates.title',
    'relation' => array('type' => 'hasOne', 'load' => 'lazy'),
    'eval' => array('mandatory' => false, 'includeBlankOption' => true, 'tl_class' => 'clr long'),
    'sql' => "int(10) unsigned NOT NULL default '0'"
);

$GLOBALS['TL_DCA']['tl_nc_language']['palettes']['mailjet'] = '{general_legend},language,fallback;{mailjet_legend},mailjet_template;{attachments_legend},attachments,attachment_templates,attachment_tokens';
