<?php
/**
 * @package   MailJetBundle
 * @author    Rory Zünd, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

$GLOBALS['TL_DCA']['tl_nc_message']['palettes']['mailjet'] = '{title_legend},title,gateway;{languages_legend},languages;{publish_legend},published';
