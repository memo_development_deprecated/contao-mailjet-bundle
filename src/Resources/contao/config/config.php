<?php
/**
 * @package   MailJetBundle
 * @author    Rory Zünd, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

/**
 * Add back end modules
 */

$GLOBALS['BE_MOD']['mod_mailjet'] = array
(
    'mailjet-apis' => array
    (
        'tables' => array('tl_mailjet_api')
    ),
    'mailjet-templates' => array
    (
        'tables' => array('tl_mailjet_transactional_templates'),
        'sync' => array('Memo\MailJetBundle\Service\MailJetHandler', 'syncTemplates'),
    ),
    'mailjet-contactlists' => array
    (
        'tables' => array('tl_mailjet_contactlists'),
        'sync' => array('Memo\MailJetBundle\Service\MailJetHandler', 'syncContactlists'),
    ),
    'mailjet-integrations' => array
    (
        'tables' => array('tl_mailjet_integration')
    ),
    'mailjet-optins' => array
    (
        'tables' => array('tl_mailjet_optin')
    )
);


/**
 * Add front end modules and Custom Elements
 */

$GLOBALS['TL_CTE']['mailjet']['mailjetregistration'] = '\Memo\MailJetBundle\Elements\MailJetRegistrationElement';
$GLOBALS['TL_CTE']['mailjet']['mailjetoptin'] = '\Memo\MailJetBundle\Elements\MailJetOptInElement';
$GLOBALS['TL_CTE']['mailjet']['mailjetmassmailoptout'] = '\Memo\MailJetBundle\Elements\MailJetMassmailOptOutElement';

/**
 * Models
 */
$GLOBALS['TL_MODELS']['tl_mailjet_api'] = 'Memo\MailJetBundle\Model\MailJetAPIModel';
$GLOBALS['TL_MODELS']['tl_mailjet_transactional_templates'] = 'Memo\MailJetBundle\Model\MailJetTransactionalTemplatesModel';
$GLOBALS['TL_MODELS']['tl_mailjet_integration'] = 'Memo\MailJetBundle\Model\MailJetIntegrationModel';
$GLOBALS['TL_MODELS']['tl_mailjet_optin'] = 'Memo\MailJetBundle\Model\MailJetOptinModel';
$GLOBALS['TL_MODELS']['tl_mailjet_recipients_archiv'] = 'Memo\MailJetBundle\Model\MailJetRecipientsArchivModel';
$GLOBALS['TL_MODELS']['tl_mailjet_recipients'] = 'Memo\MailJetBundle\Model\MailJetRecipientsModel';
$GLOBALS['TL_MODELS']['tl_mailjet_massmailing'] = 'Memo\MailJetBundle\Model\MailJetMassmailingModel';
$GLOBALS['TL_MODELS']['tl_mailjet_contactlists'] = 'Memo\MailJetBundle\Model\MailJetContactlistModel';

/**
 * HOOKS
 */

/**
 * Backend
 */

use Contao\System;
use Symfony\Component\HttpFoundation\Request;

if (System::getContainer()->get('contao.routing.scope_matcher')
    ->isBackendRequest(System::getContainer()->get('request_stack')->getCurrentRequest() ?? Request::create(''))
) {
    $GLOBALS['TL_CSS'][] = 'bundles/memomailjet/backend.css?v=2023';
    $GLOBALS['TL_JAVASCRIPT'][] = 'bundles/memomailjet/backend.js?v=2023';
}

/**
 * Forms
 */
$GLOBALS['TL_FFL']['mailjet_contactlists'] = 'Memo\MailJetBundle\FormField\MailJetContactlists';

/**
 * Inject Gateway
 */
$GLOBALS['NOTIFICATION_CENTER']['GATEWAY']['mailjet'] = Memo\MailJetBundle\Gateway\Mailjet::class;
