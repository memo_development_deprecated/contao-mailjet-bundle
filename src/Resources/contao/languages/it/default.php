<?php
/**
 * @package   MailJetBundle
 * @author    Rory Zünd, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

// Template-Content

$GLOBALS['TL_LANG']['mailjet']['confirm-optout-title'] = "Conferma l'annullamento dell'iscrizione all'e-mail";
$GLOBALS['TL_LANG']['mailjet']['confirm-optout-text'] = "Si prega di confermare ancora una volta che non si desidera ricevere e-mail automatiche da NeoVac ATA AG in futuro.";
$GLOBALS['TL_LANG']['mailjet']['confirm-optout-button'] = "Conferma logout";

$GLOBALS['TL_LANG']['mailjet']['optout-fail-title'] = "Logoff non riuscito";
$GLOBALS['TL_LANG']['mailjet']['optout-fail-text'] = "La tua cancellazione della tua e-mail purtroppo non ha avuto successo.<br>Tenta di nuovo o contattaci a <a href='mailto:info@neovac.ch'>info@neovac.ch</a>";
$GLOBALS['TL_LANG']['mailjet']['optout-fail-button'] = "alla pagina iniziale";

$GLOBALS['TL_LANG']['mailjet']['optout-success-title'] = "La cancellazione della registrazione ha avuto successo";
$GLOBALS['TL_LANG']['mailjet']['optout-success-text'] = "La tua cancellazione della tua e-mail ha avuto successo! <br>Non riceverai più e-mail da parte nostra in futuro.";
$GLOBALS['TL_LANG']['mailjet']['optout-success-button'] = "alla pagina iniziale";
