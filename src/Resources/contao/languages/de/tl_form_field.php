<?php
/**
 * @package   MailJetBundle
 * @author    Rory Zünd, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

/**
 * Fieldtypes
 */
$GLOBALS['TL_LANG']['FFL']['mailjet_contactlists'] = array("Mailjet Kontaktlistenauswahl");

/**
 * Legends
 */
$GLOBALS['TL_LANG']['tl_form_field']['mailjet_legend'] = "MailJet Optionen";

/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_form_field']['mailjetfield'] = array('Feld in MailJet speichern?', 'Soll das Feld an MailJet übermittelt werden?');
$GLOBALS['TL_LANG']['tl_form_field']['mailjetfieldname'] = array('MailJet-Feldname', 'Wie heisst das Feld in MailJet? (z.B. vorname)');
