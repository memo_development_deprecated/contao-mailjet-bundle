<?php
/**
 * @package   MailJetBundle
 * @author    Rory Zünd, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

/**
 * Legends
 */
$GLOBALS['TL_LANG']['tl_settings']['mailjet_legend'] = 'Mailjet Einstellungen';

/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_settings']['fields']['mailjet_dns_check'] = array("DNS Check", "Soll beim Anmeldeprozess die Mail-Adresse mittels DNS-Lookup kontrolliert werden?");
