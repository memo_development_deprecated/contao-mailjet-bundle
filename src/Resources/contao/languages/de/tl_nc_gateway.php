<?php
/**
 * @package   MailJetBundle
 * @author    Rory Zünd, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

$GLOBALS['TL_LANG']['tl_nc_gateway']['type']['mailjet'] = 'Mailjet';

$GLOBALS['TL_LANG']['tl_nc_gateway']['mailjet_api'] = array('Mailjet API', 'Wählen Sie die vorkonfigurierte API aus (unter Mailjet > APIs).');
