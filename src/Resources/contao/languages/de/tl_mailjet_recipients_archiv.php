<?php
/**
 * @package   MailJetBundle
 * @author    Rory Zünd, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */


/**
 * Legends
 */

$GLOBALS['TL_LANG']['tl_mailjet_recipients_archiv']['general_legend'] = "Generelle Informationen";

/**
 * Fields
 */

$GLOBALS['TL_LANG']['tl_mailjet_recipients_archiv']['title'] = array("Titel", "Definieren Sie den Titel der Empfängergruppe");

/**
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_mailjet_recipients_archiv']['new'] = array('Neue Empfängergruppe', 'Neue Empfängergruppe anlegen');
$GLOBALS['TL_LANG']['tl_mailjet_recipients_archiv']['show'] = array('Details', 'Infos zur Empfängergruppe mit der ID %s');
$GLOBALS['TL_LANG']['tl_mailjet_recipients_archiv']['edit'] = array('Empfänger bearbeiten ', 'Empfänger bearbeiten');
$GLOBALS['TL_LANG']['tl_mailjet_recipients_archiv']['editheader'] = array('Empfängergruppe bearbeiten ', 'Empfängergruppe bearbeiten');
$GLOBALS['TL_LANG']['tl_mailjet_recipients_archiv']['cut'] = array('Empfängergruppe Verschieben', 'ID %s verschieben');
$GLOBALS['TL_LANG']['tl_mailjet_recipients_archiv']['copy'] = array('Empfängergruppe Duplizieren ', 'ID %s duplizieren');
$GLOBALS['TL_LANG']['tl_mailjet_recipients_archiv']['delete'] = array('Empfängergruppe Löschen ', 'ID %s löschen');
