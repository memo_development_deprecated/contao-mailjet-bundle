<?php
/**
 * @package   MailJetBundle
 * @author    Rory Zünd, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */


/**
 * Legends
 */

$GLOBALS['TL_LANG']['tl_mailjet_massmailing']['general_legend'] = "Generelle Informationen";
$GLOBALS['TL_LANG']['tl_mailjet_massmailing']['recipient_legend'] = "Empfänger Einstellungen";
$GLOBALS['TL_LANG']['tl_mailjet_massmailing']['mail_legend'] = "Mail Details";
$GLOBALS['TL_LANG']['tl_mailjet_massmailing']['opt_out_legend'] = "Opt-Out Details";
$GLOBALS['TL_LANG']['tl_mailjet_massmailing']['expert_legend'] = "Experten Einstellungen";


/**
 * Fields
 */

$GLOBALS['TL_LANG']['tl_mailjet_massmailing']['title'] = array("Titel", "Definieren Sie den Titel des Massenmails (nur für Backend)");
$GLOBALS['TL_LANG']['tl_mailjet_massmailing']['api_id'] = array("API", "Welches API sollte verwendet werden?");
$GLOBALS['TL_LANG']['tl_mailjet_massmailing']['mail_template_id'] = array("Transaktions-Email Vorlage ID", "Auf Basis welcher E-Mail Vorlage sollte das E-Mail verschickt werden?");
$GLOBALS['TL_LANG']['tl_mailjet_massmailing']['sender_email'] = array("Absender E-Mail Adresse", "Von welcher E-Mail Adresse aus sollte das Massenmail E-Mail verschickt werden?");
$GLOBALS['TL_LANG']['tl_mailjet_massmailing']['sender_name'] = array("Absender Name", "Von welchem Namen aus sollte das Massenmail E-Mail verschickt werden?");
$GLOBALS['TL_LANG']['tl_mailjet_massmailing']['subject'] = array("Betreff", "Welchen Betreff sollte das Massenmail E-Mail haben?");
$GLOBALS['TL_LANG']['tl_mailjet_massmailing']['published'] = array("Veröffentlichen", "Soll dieses Formular öffentlich sein?");
$GLOBALS['TL_LANG']['tl_mailjet_massmailing']['sent'] = array("Verschickt", "Wurde das E-Mail verschickt?");
$GLOBALS['TL_LANG']['tl_mailjet_massmailing']['tstamp_sent'] = array("Versand-Zeitpunkt", "Wann wurde das E-Mail geschickt?");
$GLOBALS['TL_LANG']['tl_mailjet_massmailing']['recipient_archiv_id'] = array("Empfängergruppe", "An welche Empfängergruppe soll das E-Mail verschickt werden?");
$GLOBALS['TL_LANG']['tl_mailjet_massmailing']['opt_out_page'] = array("Abmelde-Seite", "Auf welcher Seite ist das 'Massenmail Opt-Out' Element hinterlegt?");
$GLOBALS['TL_LANG']['tl_mailjet_massmailing']['opt_in_page'] = array("Anmelde-Seite", "Auf welcher Seite ist das 'Opt-In' Element hinterlegt? -> wenn ausgewählt, wird dem E-Mail ein Opt-In URL beigefügt und pro Empfänger ein Opt-In Eintrag angelegt.");
$GLOBALS['TL_LANG']['tl_mailjet_massmailing']['integration_id'] = array("Integration", "Zwingend nötig, wenn ein Opt-In durchgeführt werden soll (auch Anmelde-Seite definieren)");

/**
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_mailjet_massmailing']['new'] = array('Neues Massenmail', 'Neues Massenmail anlegen');
$GLOBALS['TL_LANG']['tl_mailjet_massmailing']['show'] = array('Details', 'Infos zum Massenmail mit der ID %s');
$GLOBALS['TL_LANG']['tl_mailjet_massmailing']['edit'] = array('Massenmail bearbeiten ', 'Massenmail bearbeiten');
$GLOBALS['TL_LANG']['tl_mailjet_massmailing']['cut'] = array('Massenmail Verschieben', 'ID %s verschieben');
$GLOBALS['TL_LANG']['tl_mailjet_massmailing']['copy'] = array('Massenmail Duplizieren ', 'ID %s duplizieren');
$GLOBALS['TL_LANG']['tl_mailjet_massmailing']['delete'] = array('Massenmail Löschen ', 'ID %s löschen');
$GLOBALS['TL_LANG']['tl_mailjet_massmailing']['send'] = array('Massenmail verschicken ', 'Mail an alle Empfänger schicken');
