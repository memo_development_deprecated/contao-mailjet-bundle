<?php
/**
 * @package   MailJetBundle
 * @author    Rory Zünd, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

/**
 * Legends
 */

$GLOBALS['TL_LANG']['tl_mailjet_transactional_templates']['general_legend'] = "MailJet Transaktional-Templates Details";

/**
 * Fields
 */

$GLOBALS['TL_LANG']['tl_mailjet_transactional_templates']['title'] = array("Titel", "Sie können den importierten Namen anpassen, falls Ihnen dies beim suchen/finden hilft.");
$GLOBALS['TL_LANG']['tl_mailjet_transactional_templates']['orig_title'] = array("MailJet Titel", "Die ursprüngliche Mailjet Bezeichnung");
$GLOBALS['TL_LANG']['tl_mailjet_transactional_templates']['template_id'] = array("Template-ID", "Die ursprüngliche Mailjet Template-ID");
$GLOBALS['TL_LANG']['tl_mailjet_transactional_templates']['api_id'] = array("API", "Über welche API wurde dieses Template importiert?");
$GLOBALS['TL_LANG']['tl_mailjet_transactional_templates']['published'] = array("Aktiviert", "Soll diese Template für Aktionen verfügbar sein?");
$GLOBALS['TL_LANG']['tl_mailjet_transactional_templates']['sender_name'] = array("Absender Name", "Absender Name");
$GLOBALS['TL_LANG']['tl_mailjet_transactional_templates']['sender_email'] = array("Absender Email", "Absender Email");
$GLOBALS['TL_LANG']['tl_mailjet_transactional_templates']['subject'] = array("Betreff", "Betreff");
$GLOBALS['TL_LANG']['tl_mailjet_transactional_templates']['template_code'] = array("HTML-Code", "Generierter HTML-Code aus dem MJML-Code");
$GLOBALS['TL_LANG']['tl_mailjet_transactional_templates']['variables'] = array("Variablen in Template", "Variablen die im HTML-Code vorkommen, ein Formular muss diese Felder auch beinhalten, sonst funktioniert der Versand nicht!");

/**
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_mailjet_transactional_templates']['new'] = array('Neues Template', 'Neues Template anlegen');
$GLOBALS['TL_LANG']['tl_mailjet_transactional_templates']['show'] = array('Details', 'Infos zum Template mit der ID %s');
$GLOBALS['TL_LANG']['tl_mailjet_transactional_templates']['edit'] = array('Template bearbeiten ', 'Template bearbeiten');
$GLOBALS['TL_LANG']['tl_mailjet_transactional_templates']['cut'] = array('Template Verschieben', 'ID %s verschieben');
$GLOBALS['TL_LANG']['tl_mailjet_transactional_templates']['copy'] = array('Template Duplizieren ', 'ID %s duplizieren');
$GLOBALS['TL_LANG']['tl_mailjet_transactional_templates']['delete'] = array('Template Löschen ', 'ID %s löschen');
