<?php
/**
 * @package   MailJetBundle
 * @author    Rory Zünd, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

/**
 * Miscellaneous
 */
$GLOBALS['TL_LANG']['MSC'][''] = '';

$GLOBALS['TL_LANG']['MSC']['sync'] = 'von Mailjet synchronisieren';

// Typ Title
$GLOBALS['TL_LANG']['CTE']['mailjet'] = "MailJet";


// Content elements
$GLOBALS['TL_LANG']['CTE']['mailjetregistration'] = ['MailJet Registrierungsformular', 'Formular zur Übergabe an MailJet API'];
$GLOBALS['TL_LANG']['CTE']['mailjetoptin'] = ['MailJet Opt-In', 'Ausgabe des Opt-In Resultates'];
$GLOBALS['TL_LANG']['CTE']['mailjetmassmailoptout'] = ['MailJet Massenmail Opt-Out', 'Ausgabe des Opt-Out Resultates'];


// Template-Content
$GLOBALS['TL_LANG']['mailjet']['confirm-optout-title'] = "E-Mail Abmeldung bestätigen";
$GLOBALS['TL_LANG']['mailjet']['confirm-optout-text'] = "Bitte bestätigen Sie nochmals kurz, dass Sie zukünftig keine automatisierte E-Mails<br> von und erhalten möchten.";
$GLOBALS['TL_LANG']['mailjet']['confirm-optout-button'] = "Abmeldung bestätigen";

$GLOBALS['TL_LANG']['mailjet']['optout-fail-title'] = "Abmeldung nicht erfolgreich";
$GLOBALS['TL_LANG']['mailjet']['optout-fail-text'] = "Ihre E-Mail Abmeldung war leider nicht erfolgreich.<br>Bitte versuchen Sie die Abmeldung erneut oder wenden Sie sich an uns.";
$GLOBALS['TL_LANG']['mailjet']['optout-fail-button'] = "zur Startseite";

$GLOBALS['TL_LANG']['mailjet']['optout-double'] = "Abmeldung bereits durchgeführt";

$GLOBALS['TL_LANG']['mailjet']['optout-success-title'] = "Abmeldung erfolgreich";
$GLOBALS['TL_LANG']['mailjet']['optout-success-text'] = "Ihre E-Mail Abmeldung war erfolgreich!<br>Sie erhalten zukünftig keine E-Mails mehr von uns.";
$GLOBALS['TL_LANG']['mailjet']['optout-success-button'] = "zur Startseite";

$GLOBALS['TL_LANG']['mailjet']['optin-fail-title'] = "Anmeldung nicht erfolgreich";
$GLOBALS['TL_LANG']['mailjet']['optin-fail-text'] = "Ihre Anmeldung zum Newsletter konnte nicht übermittelt werden.<br>Bitte füllen Sie die Anmeldung zum Newsletter erneut aus. Falls das Problem weiter besteht, wenden Sie sich bitte an uns.";
$GLOBALS['TL_LANG']['mailjet']['optin-fail-button'] = "zur Startseite";

$GLOBALS['TL_LANG']['mailjet']['optin-success-title'] = "Anmeldung erfolgreich";
$GLOBALS['TL_LANG']['mailjet']['optin-success-text'] = "Sie haben die Newsletter Anmeldung erfolgreich abgeschlossen.<br>Es freut uns, Sie künftig mit aktuellen Informationen aus unserem Hause auf dem Laufenden zu halten.<br>Sie erhalten zukünftig regelmässig spannende und nützliche Nachrichten von uns.";
$GLOBALS['TL_LANG']['mailjet']['optin-success-button'] = "zur Startseite";

$GLOBALS['TL_LANG']['mailjet']['optin-confirm-title'] = "Anmeldung fast geschafft";
$GLOBALS['TL_LANG']['mailjet']['optin-confirm-text'] = "Sie erhalten bald ein E-Mail zur Bestätigung Ihrer Anmeldung.";
$GLOBALS['TL_LANG']['mailjet']['optin-confirm-button'] = "zur Startseite";

$GLOBALS['TL_LANG']['mailjet']['form-fail-text'] = "Die Newsletter Anmeldung war leider nicht erfolgreich.";
$GLOBALS['TL_LANG']['mailjet']['wrong-email'] = "Die verwendete E-Mail Adresse ist ungültig.";
$GLOBALS['TL_LANG']['mailjet']['no-email'] = "Das Pflichtfeld 'email' ist nicht vorhanden!";
