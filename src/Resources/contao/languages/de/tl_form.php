<?php
/**
 * @package   MailJetBundle
 * @author    Rory Zünd, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */


/**
 * Fields
 */

$GLOBALS['TL_LANG']['tl_form']['mailjettemplate'] = array('Mailjet Benachrichtigung', 'Soll das Formular via transaktionalem Template verschickt werden?');
