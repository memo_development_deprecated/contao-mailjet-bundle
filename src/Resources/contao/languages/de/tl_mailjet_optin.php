<?php
/**
 * @package   MailJetBundle
 * @author    Rory Zünd, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */


/**
 * Legends
 */

$GLOBALS['TL_LANG']['tl_mailjet_optin']['general_legend'] = "Opt-In Details";

/**
 * Fields
 */

$GLOBALS['TL_LANG']['tl_mailjet_optin']['email'] = array("E-Mail", "Mail-Adresse der vermeintlichen Empfängers");
$GLOBALS['TL_LANG']['tl_mailjet_optin']['email_url'] = array("E-Mail (für die URL)", "Mail-Adresse der vermeintlichen Empfängers, vorbereitet für die URL");
$GLOBALS['TL_LANG']['tl_mailjet_optin']['formfields'] = array("Formularfelder", "Formularfelder dieser Anmeldung");
$GLOBALS['TL_LANG']['tl_mailjet_optin']['hash'] = array("Verifizierungs-Hash", "Hash der zur Verifizierung verwendet wird");
$GLOBALS['TL_LANG']['tl_mailjet_optin']['registry_tstamp'] = array("Registrierungs-Zeitpunkt", "Wann hat der Kunde sich registriert? (und das Opt-In ausgelöst)");
$GLOBALS['TL_LANG']['tl_mailjet_optin']['sent'] = array("Opt-In verschickt", "Wurde das Opt-In Mail verschickt?");
$GLOBALS['TL_LANG']['tl_mailjet_optin']['sent_tstamp'] = array("Opt-In Versand Zeitpunkt", "Wann wurde das Opt-In Mail an den Kunden geschickt?");
$GLOBALS['TL_LANG']['tl_mailjet_optin']['activated'] = array("Opt-In durchgeführt", "Hat der Kunde den Opt-In Bestätigen-Button angeklickt?");
$GLOBALS['TL_LANG']['tl_mailjet_optin']['activated_tstamp'] = array("Opt-In Zeitpunkt", "Wann hat der Kunde auf den Bestätigen-Button geklickt?");
$GLOBALS['TL_LANG']['tl_mailjet_optin']['activated_ip'] = array("Opt-In IP-Adresse", "Von wo aus wurde auf den Link geklickt?");
$GLOBALS['TL_LANG']['tl_mailjet_optin']['activated_useragent'] = array("Opt-In Useragent", "Mit welchem Browser wurde auf den Link geklickt?");
$GLOBALS['TL_LANG']['tl_mailjet_optin']['mailjet_integration_id'] = array("Formular", "Mit welchem Formular wurde die Anmeldung ausgelöst?");
$GLOBALS['TL_LANG']['tl_mailjet_optin']['opened'] = array("Link geöffnet", "Hat der Kunde den Opt-In Link geöffnet?");
$GLOBALS['TL_LANG']['tl_mailjet_optin']['opened_tstamp'] = array("Link geöffnet Zeitpunkt", "Wann hat der Kunde auf den Link geöffnet?");
$GLOBALS['TL_LANG']['tl_mailjet_optin']['contactlists'] = array("Kontaktliste(n)", "Welcher/n Kontaktliste/n soll dieser Empfänger zugewiesen werden?");
$GLOBALS['TL_LANG']['tl_mailjet_optin']['optin_type'] = array("Opt-In Typ", "Welcher Opt-In Typ soll verwendet werden?");

$GLOBALS['TL_LANG']['tl_mailjet_optin']['optin_types']['double'] = 'Double-Opt-In';
$GLOBALS['TL_LANG']['tl_mailjet_optin']['optin_types']['triple'] = 'Triple-Opt-In';
$GLOBALS['TL_LANG']['tl_mailjet_optin']['optin_types']['none'] = 'Ohne Double-Opt-In';

/**
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_mailjet_optin']['new'] = array('Neuer Opt-In', 'Neuer Opt-In anlegen');
$GLOBALS['TL_LANG']['tl_mailjet_optin']['show'] = array('Details', 'Infos zum Opt-In mit der ID %s');
$GLOBALS['TL_LANG']['tl_mailjet_optin']['edit'] = array('Opt-In bearbeiten ', 'Opt-In bearbeiten');
$GLOBALS['TL_LANG']['tl_mailjet_optin']['cut'] = array('Opt-In Verschieben', 'ID %s verschieben');
$GLOBALS['TL_LANG']['tl_mailjet_optin']['copy'] = array('Opt-In Duplizieren ', 'ID %s duplizieren');
$GLOBALS['TL_LANG']['tl_mailjet_optin']['delete'] = array('Opt-In Löschen ', 'ID %s löschen');
