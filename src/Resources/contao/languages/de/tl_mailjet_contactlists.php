<?php
/**
 * @package   MailJetBundle
 * @author    Rory Zünd, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

/**
 * Legends
 */

$GLOBALS['TL_LANG']['tl_mailjet_contactlists']['general_legend'] = "MailJet Kontaktlisten Details";
$GLOBALS['TL_LANG']['tl_mailjet_contactlists']['content_legend'] = "Auswahl Option Details";
$GLOBALS['TL_LANG']['tl_mailjet_contactlists']['expert_legend'] = "Experteneinstellungen";

/**
 * Fields
 */

$GLOBALS['TL_LANG']['tl_mailjet_contactlists']['title'] = array("Titel", "Sie können den importierten Namen anpassen, falls Ihnen dies beim suchen/finden hilft.");
$GLOBALS['TL_LANG']['tl_mailjet_contactlists']['orig_title'] = array("MailJet Titel", "Die ursprüngliche Mailjet Bezeichnung");
$GLOBALS['TL_LANG']['tl_mailjet_contactlists']['contactlist_id'] = array("Kontaktlisten-ID", "Die ursprüngliche Mailjet Kontaktlisten-ID");
$GLOBALS['TL_LANG']['tl_mailjet_contactlists']['api_id'] = array("API", "Über welche API wurde diese Kontaktliste importiert?");

$GLOBALS['TL_LANG']['tl_mailjet_contactlists']['headline'] = array("Überschrift", "Überschrift für die Auswahl Option");
$GLOBALS['TL_LANG']['tl_mailjet_contactlists']['subheadline'] = array("Untertitel", "Untertitel für die Auswahl Option");
$GLOBALS['TL_LANG']['tl_mailjet_contactlists']['teaser'] = array("Teaser", "Teaser ohne Formatierung für die Auswahl Option");
$GLOBALS['TL_LANG']['tl_mailjet_contactlists']['text'] = array("Text", "Text mit Formatierung für die Auswahl Option");
$GLOBALS['TL_LANG']['tl_mailjet_contactlists']['image'] = array("Bild", "Bild für die Auswahl Option");
$GLOBALS['TL_LANG']['tl_mailjet_contactlists']['show_as_option'] = array("Als Auswahl Option anzeigen", "Soll diese Kontaktliste als Auswahl-Option bei der Anmeldung erscheinen?");


$GLOBALS['TL_LANG']['tl_mailjet_contactlists']['published'] = array("Aktiviert", "Soll diese Kontaktliste für Aktionen verfügbar sein?");

/**
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_mailjet_contactlists']['new'] = array('Neue Kontaktliste', 'Neue Kontaktliste anlegen');
$GLOBALS['TL_LANG']['tl_mailjet_contactlists']['show'] = array('Details', 'Infos zur Kontaktliste mit der ID %s');
$GLOBALS['TL_LANG']['tl_mailjet_contactlists']['edit'] = array('Kontaktliste bearbeiten ', 'Kontaktliste bearbeiten');
$GLOBALS['TL_LANG']['tl_mailjet_contactlists']['cut'] = array('Kontaktliste Verschieben', 'ID %s verschieben');
$GLOBALS['TL_LANG']['tl_mailjet_contactlists']['copy'] = array('Kontaktliste Duplizieren ', 'ID %s duplizieren');
$GLOBALS['TL_LANG']['tl_mailjet_contactlists']['delete'] = array('Kontaktliste Löschen ', 'ID %s löschen');
