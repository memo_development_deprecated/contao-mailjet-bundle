<?php
/**
 * @package   MailJetBundle
 * @author    Rory Zünd, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */


/**
 * Legends
 */

$GLOBALS['TL_LANG']['tl_mailjet_recipients']['general_legend'] = "Personendaten";
$GLOBALS['TL_LANG']['tl_mailjet_recipients']['variable_legend'] = "Variablen";
$GLOBALS['TL_LANG']['tl_mailjet_recipients']['expert_legend'] = "Experteneinstellungen";

/**
 * Fields
 */

$GLOBALS['TL_LANG']['tl_mailjet_recipients']['firstname'] = array("Vorname", "Definieren Sie den Vornamen des Empfängers");
$GLOBALS['TL_LANG']['tl_mailjet_recipients']['lastname'] = array("Nachname", "Definieren Sie den Nachnamen des Empfängers");
$GLOBALS['TL_LANG']['tl_mailjet_recipients']['email'] = array("E-Mail", "Definieren Sie die E-Mail Adresse des Empfängers");
$GLOBALS['TL_LANG']['tl_mailjet_recipients']['published'] = array("Aktiv", "Ist der Empfänger aktiv? Falls der Empfänger den 'Abmelden Link' anklickt, wird dieses Feld deaktiviert.");

$GLOBALS['TL_LANG']['tl_mailjet_recipients']['url_email'] = array("URL E-Mail", "E-Mail für Opt-Out");
$GLOBALS['TL_LANG']['tl_mailjet_recipients']['hash'] = array("Hash", "Hash für Opt-Out");
$GLOBALS['TL_LANG']['tl_mailjet_recipients']['sent'] = array("Massenmail verschickt", "Wurde das Massenmail an diesen Empfänger geschickt?");
$GLOBALS['TL_LANG']['tl_mailjet_recipients']['tstamp_sent'] = array("Versand-Zeitpunkt", "Wann wurde das E-Mail geschickt");
$GLOBALS['TL_LANG']['tl_mailjet_recipients']['variable1'] = array("Variabel 1", "Dynamisches Feld für die Personalisierung der E-Mails. Verwenden Sie den Variabelnnamen variable1 in der Transaktionalen Vorlage. Dann wird der hier hinterlegte Wert im Mail eingesetzt.");
$GLOBALS['TL_LANG']['tl_mailjet_recipients']['variable2'] = array("Variabel 2", "Dynamisches Feld für die Personalisierung der E-Mails. Verwenden Sie den Variabelnnamen variable2 in der Transaktionalen Vorlage. Dann wird der hier hinterlegte Wert im Mail eingesetzt.");
$GLOBALS['TL_LANG']['tl_mailjet_recipients']['variable3'] = array("Variabel 3", "Dynamisches Feld für die Personalisierung der E-Mails. Verwenden Sie den Variabelnnamen variable3 in der Transaktionalen Vorlage. Dann wird der hier hinterlegte Wert im Mail eingesetzt.");
$GLOBALS['TL_LANG']['tl_mailjet_recipients']['variable4'] = array("Variabel 4", "Dynamisches Feld für die Personalisierung der E-Mails. Verwenden Sie den Variabelnnamen variable4 in der Transaktionalen Vorlage. Dann wird der hier hinterlegte Wert im Mail eingesetzt.");
$GLOBALS['TL_LANG']['tl_mailjet_recipients']['variable5'] = array("Variabel 5", "Dynamisches Feld für die Personalisierung der E-Mails. Verwenden Sie den Variabelnnamen variable5 in der Transaktionalen Vorlage. Dann wird der hier hinterlegte Wert im Mail eingesetzt.");
$GLOBALS['TL_LANG']['tl_mailjet_recipients']['variable6'] = array("Variabel 6", "Dynamisches Feld für die Personalisierung der E-Mails. Verwenden Sie den Variabelnnamen variable6 in der Transaktionalen Vorlage. Dann wird der hier hinterlegte Wert im Mail eingesetzt.");
$GLOBALS['TL_LANG']['tl_mailjet_recipients']['variable7'] = array("Variabel 7", "Dynamisches Feld für die Personalisierung der E-Mails. Verwenden Sie den Variabelnnamen variable7 in der Transaktionalen Vorlage. Dann wird der hier hinterlegte Wert im Mail eingesetzt.");
$GLOBALS['TL_LANG']['tl_mailjet_recipients']['variable8'] = array("Variabel 8", "Dynamisches Feld für die Personalisierung der E-Mails. Verwenden Sie den Variabelnnamen variable8 in der Transaktionalen Vorlage. Dann wird der hier hinterlegte Wert im Mail eingesetzt.");
$GLOBALS['TL_LANG']['tl_mailjet_recipients']['variable9'] = array("Variabel 9", "Dynamisches Feld für die Personalisierung der E-Mails. Verwenden Sie den Variabelnnamen variable9 in der Transaktionalen Vorlage. Dann wird der hier hinterlegte Wert im Mail eingesetzt.");
$GLOBALS['TL_LANG']['tl_mailjet_recipients']['variable10'] = array("Variabel 10", "Dynamisches Feld für die Personalisierung der E-Mails. Verwenden Sie den Variabelnnamen variable10 in der Transaktionalen Vorlage. Dann wird der hier hinterlegte Wert im Mail eingesetzt.");
/**
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_mailjet_recipients']['new'] = array('Neuer Empfänger', 'Neuer Empfänger anlegen');
$GLOBALS['TL_LANG']['tl_mailjet_recipients']['show'] = array('Details', 'Infos zum Empfänger mit der ID %s');
$GLOBALS['TL_LANG']['tl_mailjet_recipients']['edit'] = array('Empfänger bearbeiten ', 'Empfänger bearbeiten');
$GLOBALS['TL_LANG']['tl_mailjet_recipients']['cut'] = array('Empfänger Verschieben', 'ID %s verschieben');
$GLOBALS['TL_LANG']['tl_mailjet_recipients']['copy'] = array('Empfänger Duplizieren ', 'ID %s duplizieren');
$GLOBALS['TL_LANG']['tl_mailjet_recipients']['delete'] = array('Empfänger Löschen ', 'ID %s löschen');
