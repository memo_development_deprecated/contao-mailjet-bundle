<?php
/**
 * @package   MailJetBundle
 * @author    Rory Zünd, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

/**
 * Front end modules
 */

$GLOBALS['TL_LANG']['MOD']['mod_mailjet'] = array('MailJet');
$GLOBALS['TL_LANG']['MOD']['mailjet-apis'] = array('APIs');
$GLOBALS['TL_LANG']['MOD']['mailjet-templates'] = array('Trans. Vorlagen');
$GLOBALS['TL_LANG']['MOD']['mailjet-integrations'] = array('Integrationen');
$GLOBALS['TL_LANG']['MOD']['mailjet-optins'] = array('Opt-Ins');
$GLOBALS['TL_LANG']['MOD']['mailjet-recipients'] = array('Empfänger');
$GLOBALS['TL_LANG']['MOD']['mailjet-massmailing'] = array('Massenmails');
$GLOBALS['TL_LANG']['MOD']['mailjet-contactlists'] = array('Kontaktlisten');
