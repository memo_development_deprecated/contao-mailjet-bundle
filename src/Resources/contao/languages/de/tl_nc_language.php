<?php
/**
 * @package   MailJetBundle
 * @author    Rory Zünd, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

$GLOBALS['TL_LANG']['tl_nc_language']['mailjet_legend'] = 'Mailjet Transaktionsmails';

$GLOBALS['TL_LANG']['tl_nc_language']['mailjet_template'] = array('Mailjet Template', 'Hier können Sie von den zuvor importierten (unter Mailjet > Trans. Vorlagen > Sync) Templates auswählen.');
