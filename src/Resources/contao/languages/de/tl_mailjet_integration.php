<?php
/**
 * @package   MailJetBundle
 * @author    Rory Zünd, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */


/**
 * Legends
 */

$GLOBALS['TL_LANG']['tl_mailjet_integration']['general_legend'] = "Generelle Informaitonen";
$GLOBALS['TL_LANG']['tl_mailjet_integration']['registration_legend'] = "1. Schritt - Registrierungs-Formular";
$GLOBALS['TL_LANG']['tl_mailjet_integration']['optinmail_legend'] = "2. Schritt Opt-In Mail";
$GLOBALS['TL_LANG']['tl_mailjet_integration']['optin_legend'] = "3. Opt-In Bestätigungs-Seite";
$GLOBALS['TL_LANG']['tl_mailjet_integration']['result_legend'] = "4. Aktivierung & Aufnahme in Kontaktliste";
$GLOBALS['TL_LANG']['tl_mailjet_integration']['expert_legend'] = "Experten Einstellungen";

/**
 * Fields
 */

$GLOBALS['TL_LANG']['tl_mailjet_integration']['title'] = array("Titel", "Definieren Sie den Titel der Integration (nur für Backend)");
$GLOBALS['TL_LANG']['tl_mailjet_integration']['api_id'] = array("API", "Welches API sollte verwendet werden?");
$GLOBALS['TL_LANG']['tl_mailjet_integration']['type'] = array("Typ", "Integrationstyp wählen - mit oder ohne Double-Opt-In");
$GLOBALS['TL_LANG']['tl_mailjet_integration']['form_id'] = array("Formular", "Auf Basis welches Formulars sollte die Registrierung durchgeführt werden?");
$GLOBALS['TL_LANG']['tl_mailjet_integration']['mail_template_id'] = array("Transaktions-Email Vorlage", "Auf Basis welcher E-Mail Vorlage sollte das Opt-In Mail verschickt werden?");
$GLOBALS['TL_LANG']['tl_mailjet_integration']['sender_email'] = array("Absender E-Mail Adresse", "Von welcher E-Mail Adresse aus sollte das Opt-In E-Mail verschickt werden?");
$GLOBALS['TL_LANG']['tl_mailjet_integration']['sender_name'] = array("Absender Name", "Von welchem Namen aus sollte das Opt-In E-Mail verschickt werden?");
$GLOBALS['TL_LANG']['tl_mailjet_integration']['subject'] = array("Betreff", "Welchen Betreff sollte das Opt-In E-Mail haben?");
$GLOBALS['TL_LANG']['tl_mailjet_integration']['contactlisttype'] = array("Kontaktlisten-Zuweisung", "Wie wird die Kontaktliste zugewiesen?");
$GLOBALS['TL_LANG']['tl_mailjet_integration']['contactlist_id'] = array("Kontaktliste", "In welche Kontaktliste sollte die Anmeldung erfolgen?");
$GLOBALS['TL_LANG']['tl_mailjet_integration']['opt_in_page'] = array("Opt-In Seite", "Auf welche Seite sollte der Opt-In Link verweisen bzw. wo wurde/wird das Opt-In Element eingebunden?");
$GLOBALS['TL_LANG']['tl_mailjet_integration']['registered_page'] = array("Formularbestätigungs-Seite", "Auf welche Seite sollte der Opt-In Link verweisen und wo wurde das Opt-In Element eingebunden?");


$GLOBALS['TL_LANG']['tl_mailjet_integration']['published'] = array("Veröffentlichen", "Soll dieses Formular öffentlich sein?");

/**
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_mailjet_integration']['new'] = array('Neue Integration', 'Neue Integration anlegen');
$GLOBALS['TL_LANG']['tl_mailjet_integration']['show'] = array('Details', 'Infos zur Integration mit der ID %s');
$GLOBALS['TL_LANG']['tl_mailjet_integration']['edit'] = array('Integration bearbeiten ', 'Integration bearbeiten');
$GLOBALS['TL_LANG']['tl_mailjet_integration']['cut'] = array('Integration Verschieben', 'ID %s verschieben');
$GLOBALS['TL_LANG']['tl_mailjet_integration']['copy'] = array('Integration Duplizieren ', 'ID %s duplizieren');
$GLOBALS['TL_LANG']['tl_mailjet_integration']['delete'] = array('Integration Löschen ', 'ID %s löschen');

// Explanation:
$GLOBALS['TL_LANG']['tl_mailjet_integration'][''] = 'Anmeldung mit Double-Opt-In: Der Endnutzer erhält eine E-Mail mit einem Link, welchen er bestätigen muss. Anmeldung ohne Double-Opt-In: Der Endnutzer wird direkt angemeldet.';

$GLOBALS['TL_LANG']['XPL']['mailjet_integration_type'] = [
    ['Double-Opt-In', 'Der Endnutzer erhält eine E-Mail mit einem Link, welchen er öffnen muss.'],
    ['Triple-Opt-In', 'Der Endnutzer erhält eine E-Mail mit einem Link, welchen er öffnen und auf der angezeigten Seite per Button die Anmeldung bestätigen muss.'],
    ['Direkt / ohne Opt-In', 'Die Anmeldung wird direkt durchgeführt, ohne Verifizierung der E-Mail Adresse.'],
];
