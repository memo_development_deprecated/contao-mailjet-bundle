<?php
/**
 * @package   MailJetBundle
 * @author    Rory Zünd, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */


/**
 * Legends
 */

$GLOBALS['TL_LANG']['tl_mailjet_api']['general_legend'] = "MailJet API Details";

/**
 * Fields
 */

$GLOBALS['TL_LANG']['tl_mailjet_api']['title'] = array("Titel", "Definieren Sie den Titel der Schnittstelle");
$GLOBALS['TL_LANG']['tl_mailjet_api']['api_key'] = array("API Key", "Aus MailJet Unterkonto entnehmen");
$GLOBALS['TL_LANG']['tl_mailjet_api']['private_key'] = array("Private Key", "Aus MailJet Unterkonto entnehmen");
$GLOBALS['TL_LANG']['tl_mailjet_api']['published'] = array("Aktiviert", "Soll diese Schnittstelle aktiviert werden?");

/**
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_mailjet_api']['new'] = array('Neue Schnittstelle', 'Neue Schnittstelle anlegen');
$GLOBALS['TL_LANG']['tl_mailjet_api']['show'] = array('Details', 'Infos zur Schnittstelle mit der ID %s');
$GLOBALS['TL_LANG']['tl_mailjet_api']['edit'] = array('Schnittstelle bearbeiten ', 'Schnittstelle bearbeiten');
$GLOBALS['TL_LANG']['tl_mailjet_api']['cut'] = array('Schnittstelle Verschieben', 'ID %s verschieben');
$GLOBALS['TL_LANG']['tl_mailjet_api']['copy'] = array('Schnittstelle Duplizieren ', 'ID %s duplizieren');
$GLOBALS['TL_LANG']['tl_mailjet_api']['delete'] = array('Schnittstelle Löschen ', 'ID %s löschen');
