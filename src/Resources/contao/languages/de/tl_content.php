<?php
/**
 * @package   MailJetBundle
 * @author    Rory Zünd, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */


/**
 * Fields
 */


$GLOBALS['TL_LANG']['tl_content']['mailjet_api_id'] = array('MailJet API', 'Welche der MailJet Schnittstelle soll verwendet werden?');
$GLOBALS['TL_LANG']['tl_content']['mailjet_integration_id'] = array('MailJet Integration', 'Welche MailJet Integration soll verwendet werden?');
$GLOBALS['TL_LANG']['tl_content']['mailjet_optin_title'] = array('Titel', 'Was für ein Titel soll auf der Bestätigungsseite stehen, bevor auf das Formular geklickt wurde?');
$GLOBALS['TL_LANG']['tl_content']['mailjet_optin_text'] = array('Text', 'Was soll auf der Bestätigungsseite stehen, bevor auf das Formular geklickt wurde?');
$GLOBALS['TL_LANG']['tl_content']['mailjet_optin_button_text'] = array('Button-Text', 'Was soll im Bestätigungs-Button stehen? (Standard: Newsletter abonnieren)');
