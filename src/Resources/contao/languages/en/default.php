<?php
/**
 * @package   MailJetBundle
 * @author    Rory Zünd, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */


// Template-Content
$GLOBALS['TL_LANG']['mailjet']['confirm-optout-title'] = "Confirm e-mail unsubscription";
$GLOBALS['TL_LANG']['mailjet']['confirm-optout-text'] = "Please confirm again briefly that you do not wish to receive automated e-mails from us in the future.";
$GLOBALS['TL_LANG']['mailjet']['confirm-optout-button'] = "Confirm logout";

$GLOBALS['TL_LANG']['mailjet']['optout-fail-title'] = "Logoff not successful";
$GLOBALS['TL_LANG']['mailjet']['optout-fail-text'] = "Your e-mail unsubscription was unfortunately not successful.<br>Please try the unsubscription again or contact us.";
$GLOBALS['TL_LANG']['mailjet']['optout-fail-button'] = "to the homepage";

$GLOBALS['TL_LANG']['mailjet']['optout-success-title'] = "Logoff successful";
$GLOBALS['TL_LANG']['mailjet']['optout-success-text'] = "Your e-mail unsubscription was successful!<br>You will not receive any more e-mails from us in future.";
$GLOBALS['TL_LANG']['mailjet']['optout-success-button'] = "to the homepage";

$GLOBALS['TL_LANG']['mailjet']['optout-double'] = "Unsubscription already performed";

$GLOBALS['TL_LANG']['mailjet']['optin-fail-title'] = "Registration not successful";
$GLOBALS['TL_LANG']['mailjet']['optin-fail-text'] = "Unfortunately your e-mail registration was not successful.<br>Please try the registration again or contact us.";
$GLOBALS['TL_LANG']['mailjet']['optin-fail-button'] = "to the homepage";

$GLOBALS['TL_LANG']['mailjet']['optin-success-title'] = "Registration successful";
$GLOBALS['TL_LANG']['mailjet']['optin-success-text'] = "You have successfully completed the newsletter registration.<br>We look forward to keeping you up to date with the latest information from our company in the future.<br>You will regularly receive exciting and useful news from us.";
$GLOBALS['TL_LANG']['mailjet']['optin-success-button'] = "to the homepage";

$GLOBALS['TL_LANG']['mailjet']['optin-confirm-title'] = "Registration almost completed";
$GLOBALS['TL_LANG']['mailjet']['optin-confirm-text'] = "Newsletter subscription successful - you will soon receive an e-mail to confirm your e-mail address.";
$GLOBALS['TL_LANG']['mailjet']['optin-confirm-button'] = "to the homepage";

$GLOBALS['TL_LANG']['mailjet']['form-fail-text'] = "Unfortunately the newsletter registration was not successful.";
$GLOBALS['TL_LANG']['mailjet']['wrong-email'] = "The e-mail address used is invalid.";
$GLOBALS['TL_LANG']['mailjet']['no-email'] = "The mandatory field 'email' is not defined!";