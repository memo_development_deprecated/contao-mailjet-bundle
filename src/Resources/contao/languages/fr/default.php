<?php
/**
 * @package   MailJetBundle
 * @author    Rory Zünd, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */


// Template-Content

$GLOBALS['TL_LANG']['mailjet']['confirm-optout-title'] = "Confirmer le désabonnement par courriel";
$GLOBALS['TL_LANG']['mailjet']['confirm-optout-text'] = "Veuillez confirmer à nouveau que vous ne souhaitez plus recevoir d'e-mails automatisés de NeoVac ATA SA à l'avenir.";
$GLOBALS['TL_LANG']['mailjet']['confirm-optout-button'] = "Confirmer la déconnexion";

$GLOBALS['TL_LANG']['mailjet']['optout-fail-title'] = "La déconnexion n'a pas réussi";
$GLOBALS['TL_LANG']['mailjet']['optout-fail-text'] = "Votre désabonnement e-mail n'a malheureusement pas réussi. <br>Veuillez réessayer ou nous contacter à l'adresse suivante <a href='mailto:info@neovac.ch'>info@neovac.ch</a>";
$GLOBALS['TL_LANG']['mailjet']['optout-fail-button'] = "à la page d'accueil";

$GLOBALS['TL_LANG']['mailjet']['optout-success-title'] = "Radiation réussie";
$GLOBALS['TL_LANG']['mailjet']['optout-success-text'] = "Votre désinscription par e-mail a réussi!<br>Vous ne recevrez plus d'e-mails de notre part à l'avenir.";
$GLOBALS['TL_LANG']['mailjet']['optout-success-button'] = "à la page d'accueil";

$GLOBALS['TL_LANG']['mailjet']['optout-double'] = "Désinscription déjà effectuée";

$GLOBALS['TL_LANG']['mailjet']['optin-fail-title'] = "L'enregistrement n'a pas abouti";
$GLOBALS['TL_LANG']['mailjet']['optin-fail-text'] = "Malheureusement, l'enregistrement de votre e-mail n'a pas abouti.<br>Veuillez réessayer l'enregistrement ou nous contacter.";
$GLOBALS['TL_LANG']['mailjet']['optin-fail-button'] = "à la page d'accueil";

$GLOBALS['TL_LANG']['mailjet']['optin-success-title'] = "Inscription réussie";
$GLOBALS['TL_LANG']['mailjet']['optin-success-text'] = "Vous avez terminé avec succès l'inscription à la newsletter.<br>Nous sommes impatients de vous tenir au courant des dernières informations de notre entreprise à l'avenir.<br>Vous recevrez régulièrement des nouvelles passionnantes et utiles de notre part.";
$GLOBALS['TL_LANG']['mailjet']['optin-success-button'] = "à la page d'accueil";

$GLOBALS['TL_LANG']['mailjet']['optin-confirm-title'] = "L'inscription est presque terminée";
$GLOBALS['TL_LANG']['mailjet']['optin-confirm-text'] = "Inscription à la newsletter réussie - vous recevrez bientôt un e-mail pour confirmer votre adresse e-mail.";
$GLOBALS['TL_LANG']['mailjet']['optin-confirm-button'] = "à la page d'accueil";

$GLOBALS['TL_LANG']['mailjet']['form-fail-text'] = "Malheureusement, l'inscription à la newsletter n'a pas abouti.";
$GLOBALS['TL_LANG']['mailjet']['wrong-email'] = "L'adresse électronique utilisée n'est pas valide.";
$GLOBALS['TL_LANG']['mailjet']['no-email'] = "Le champ obligatoire 'email' n'est pas défini !";
