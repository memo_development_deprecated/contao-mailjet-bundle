<?php
/**
 * @package   MailJetBundle
 * @author    Rory Zünd, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */


// Template-Content
$GLOBALS['TL_LANG']['mailjet']['confirm-optout-title'] = "Uitschrijving per e-mail bevestigen";
$GLOBALS['TL_LANG']['mailjet']['confirm-optout-text'] = "Bevestig nogmaals kort dat u in de toekomst geen geautomatiseerde e-mails van ons wenst te ontvangen.";
$GLOBALS['TL_LANG']['mailjet']['confirm-optout-button'] = "Uitloggen bevestigen";

$GLOBALS['TL_LANG']['mailjet']['optout-fail-title'] = "Afmelden niet gelukt";
$GLOBALS['TL_LANG']['mailjet']['optout-fail-text'] = "Uw e-mail afmelding is helaas niet gelukt.<br>Probeer de afmelding nogmaals of neem contact met ons op.";
$GLOBALS['TL_LANG']['mailjet']['optout-fail-button'] = "naar de homepage";

$GLOBALS['TL_LANG']['mailjet']['optout-success-title'] = "Afmelden succesvol";
$GLOBALS['TL_LANG']['mailjet']['optout-success-text'] = "Uw e-mail afmelding was succesvol! <br>U zult in de toekomst geen e-mails meer van ons ontvangen.";
$GLOBALS['TL_LANG']['mailjet']['optout-success-button'] = "naar de homepage";

$GLOBALS['TL_LANG']['mailjet']['optout-double'] = "Uitschrijving reeds uitgevoerd";

$GLOBALS['TL_LANG']['mailjet']['optin-fail-title'] = "Registratie niet succesvol";
$GLOBALS['TL_LANG']['mailjet']['optin-fail-text'] = "Helaas was uw e-mail registratie niet succesvol.<br>Probeer de registratie opnieuw of neem contact met ons op.";
$GLOBALS['TL_LANG']['mailjet']['optin-fail-button'] = "naar de homepage";

$GLOBALS['TL_LANG']['mailjet']['optin-success-title'] = "Registratie succesvol";
$GLOBALS['TL_LANG']['mailjet']['optin-success-text'] = "U heeft de inschrijving voor de nieuwsbrief succesvol afgerond.<br>We kijken ernaar uit om u in de toekomst op de hoogte te houden van de laatste informatie van ons bedrijf.<br>U zult regelmatig spannend en nuttig nieuws van ons ontvangen.";
$GLOBALS['TL_LANG']['mailjet']['optin-success-button'] = "naar de homepage";

$GLOBALS['TL_LANG']['mailjet']['optin-confirm-title'] = "Registratie bijna voltooid";
$GLOBALS['TL_LANG']['mailjet']['optin-confirm-text'] = "Inschrijving op de nieuwsbrief is gelukt - u ontvangt binnenkort een e-mail om uw e-mailadres te bevestigen.";
$GLOBALS['TL_LANG']['mailjet']['optin-confirm-button'] = "naar de homepage";

$GLOBALS['TL_LANG']['mailjet']['form-fail-text'] = "Helaas was de inschrijving voor de nieuwsbrief niet succesvol.";
$GLOBALS['TL_LANG']['mailjet']['wrong-email'] = "Het gebruikte e-mailadres is ongeldig.";
$GLOBALS['TL_LANG']['mailjet']['no-email'] = "Het verplichte veld 'e-mail' is niet gedefinieerd!";
