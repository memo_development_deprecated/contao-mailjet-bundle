<?php
/**
 * @package   MailJetBundle
 * @author    Rory Zünd, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */


namespace Memo\MailJetBundle\FormField;

use Contao\FormCheckBox;
use Memo\MailJetBundle\Model\MailJetContactlistModel;

class MailJetContactlists extends FormCheckBox
{
    protected $strTemplate = 'form_mailjet_contactlists';

    public function __set($strKey, $varValue)
    {

        switch ($strKey) {
            case 'options':
                $arrOptions = array();

                // Load contactlists as options
                if ($colContactlists = MailJetContactlistModel::findBy(array('published=?', 'show_as_option=?'), array(1, 1))) {
                    foreach ($colContactlists as $objContactlist) {

                        if ($objContactlist->headline != '') {
                            $strTitle = $objContactlist->headline;
                        } elseif ($objContactlist->title != '') {
                            $strTitle = $objContactlist->title;
                        } else {
                            $strTitle = $objContactlist->orig_title;
                        }

                        $arrOptions[] = array('value' => $objContactlist->id, 'label' => $strTitle, 'object' => $objContactlist);

                    }
                }

                $this->arrOptions = $arrOptions;
                break;

            case 'rgxp':
            case 'minlength':
            case 'maxlength':
            case 'minval':
            case 'maxval':
                // Ignore
                break;

            default:
                parent::__set($strKey, $varValue);
                break;
        }
    }

    public function __get($strKey)
    {
        if ($strKey == 'options') {
            return $this->arrOptions;
        }

        return parent::__get($strKey);
    }

}
