<?php
/**
 * @package   MailJetBundle
 * @author    Rory Zünd, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

namespace Memo\MailJetBundle\Service;

use Memo\MailJetBundle\Model\MailJetRecipientsModel;

class ImportHandler
{

    public function redirectToImportForm($dc)
    {

        if ($dc->id) {
            $strReturnURL = "/contao?do=mailjet-recipients&table=tl_mailjet_recipients&id=" . $dc->id;
            $strParameter = urlencode($strReturnURL);
            header("Location: /contao/mj-import?id=" . $dc->id . "&redirect=" . $strParameter);
        } else {
            $strReturnURL = "/contao?do=mailjet-recipients";
            $strParameter = urlencode($strReturnURL);
            header("Location: /contao/mj-import?redirect=" . $strParameter);
        }

        die();
    }

    public function importRecipients($strFile, $intGroup)
    {

        //Setup-Data
        $arrFieldColumnMapping = array(
            'firstname' => array('column' => '', 'mandatory' => true),
            'lastname' => array('column' => '', 'mandatory' => true),
            'email' => array('column' => '', 'mandatory' => true),
            'variable1' => array('column' => '', 'mandatory' => false),
            'variable2' => array('column' => '', 'mandatory' => false),
            'variable3' => array('column' => '', 'mandatory' => false),
            'variable4' => array('column' => '', 'mandatory' => false),
            'variable5' => array('column' => '', 'mandatory' => false),
            'variable6' => array('column' => '', 'mandatory' => false),
            'variable7' => array('column' => '', 'mandatory' => false),
            'variable8' => array('column' => '', 'mandatory' => false),
            'variable9' => array('column' => '', 'mandatory' => false),
            'variable10' => array('column' => '', 'mandatory' => false)
        );

        // Read xlsx-file
        $objReader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $objReader->setReadDataOnly(true);
        $objReader->setLoadSheetsOnly("Tabelle1");

        if ($objSpreadsheet = $objReader->load($strFile)) {

            // Get Data from Excel-Object
            $arrData = $objSpreadsheet->getActiveSheet()->toArray(null, true, true, true);

            // Extract Header Row
            $arrHeaderRow = $arrData[1];
            unset($arrData[1]);

            // Check and map fields
            foreach ($arrFieldColumnMapping as $strField => $arrField) {
                // Mandatory Field?
                if ($strColumn = array_search($strField, $arrHeaderRow)) {
                    $arrFieldColumnMapping[$strField]['column'] = $strColumn;
                } elseif ($arrField['mandatory']) {
                    return "Folgende Pflichtspalte fehlt noch in Ihrem Excel-File: <b>" . $strField . "</b>";
                } else {
                    // This Column will be ignored...
                    unset($arrFieldColumnMapping[$strField]);
                }
            }

            // Loop Rows and Import them
            foreach ($arrData as $arrRow) {
                self::importRow($arrRow, $arrFieldColumnMapping, $intGroup);
            }

        } else {
            return "Die Datei " . basename($_FILES["file"]["name"]) . " konnte nicht eingelesen werden";
        }

        return count($arrData);

    }

    public function importRow($arrRow, $arrFieldColumnMapping, $intGroup)
    {
        // Create a new recipient with all default-values
        $objRecipient = new MailJetRecipientsModel();
        $objRecipient->tstamp = time();
        $objRecipient->published = 1;
        $objRecipient->pid = $intGroup;
        $objRecipient->tstamp_sent = 0;
        $objRecipient->sent = 0;

        // Loop the predefined mappings
        foreach ($arrFieldColumnMapping as $strField => $arrColumn) {

            // Get the column-from excel-sheet
            $strColumn = $arrColumn['column'];

            // Is there a value in the cell?
            if ($arrRow[$strColumn] != '') {
                $objRecipient->$strField = $arrRow[$strColumn];
            }
        }

        // Save the new recipient
        $objRecipient->save();
    }
}
