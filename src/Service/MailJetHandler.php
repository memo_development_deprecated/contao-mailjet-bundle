<?php
/**
 * @package   MailJetBundle
 * @author    Rory Zünd, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

namespace Memo\MailJetBundle\Service;

use Contao\Config;
use Contao\Controller;
use Contao\CoreBundle\Monolog\ContaoContext;
use Contao\FormFieldModel;
use Contao\Input;
use Contao\Message;
use Contao\System;
use Exception;
use Mailjet\Client;
use Mailjet\Resources;
use Memo\MailJetBundle\Model\MailJetAPIModel;
use Memo\MailJetBundle\Model\MailJetContactlistModel;
use Memo\MailJetBundle\Model\MailJetMassmailingModel;
use Memo\MailJetBundle\Model\MailJetOptinModel;
use Memo\MailJetBundle\Model\MailJetRecipientsModel;
use Memo\MailJetBundle\Model\MailJetTransactionalTemplatesModel;
use Psr\Log\LogLevel;

class MailJetHandler
{

    public static function addContact($objAPI, $strEmail)
    {

        // Mit API von MailJet verbinden
        $objMailjet = self::getMailjetClient($objAPI);

        // MailJet API erreichbar
        if ($objMailjet) {

            // Besteht der Kontakt schon?
            $bolContactExists = self::checkContactExist($objAPI, $strEmail);

            if (!$bolContactExists) {

                // Neuen Kontakt anlegen
                $arrBody = [
                    'Email' => $strEmail
                ];

                $objResponse = $objMailjet->post(Resources::$Contact, ['body' => $arrBody]);

                // War die Erstellun erfolgreich?
                if (!$objResponse->success()) {
                    $arrResponse = $objResponse->getData();

                    throw new Exception("Newsletter-Contact could not be created: " . $arrResponse['ErrorMessage']);

                }
            }

        } else {

            throw new Exception("Mailjet API URL or Login wrong");

        }

        return true;

    }

    public static function getMailjetClient(MailJetAPIModel $objAPI, $strVersion = 'v3')
    {

        // API Daten abholen
        $strAPIKey = $objAPI->api_key;
        $strAPISecret = $objAPI->private_key;

        // Mit API von MailJet verbinden
        $objMailjet = new Client($strAPIKey, $strAPISecret, true, ['version' => $strVersion, 'connect_timeout' => 20]);

        if ($objMailjet) {
            $objMailjet->setTimeout(20);
            return $objMailjet;
        } else {
            throw new Exception('Could not get Mailjet Client');
        }

    }

    public static function checkContactExist($objAPI, $strEmail)
    {

        // Mit API von MailJet verbinden
        $objMailjet = self::getMailjetClient($objAPI);

        // MailJet API erreichbar
        if ($objMailjet) {

            // Besteht der Kontakt schon?
            $objResponse = $objMailjet->get(Resources::$Contact, ['id' => $strEmail]);
            $arrResponse = $objResponse->getData();

            if (array_key_exists('StatusCode', $arrResponse) && $arrResponse['StatusCode'] != '200') {

                return false;

            } else {

                $arrResponse = $objResponse->getData();
                return $arrResponse;

            }


        } else {

            throw new Exception("Mailjet API URL or Login wrong");

        }

        return false;

    }

    public static function addContactProperty($objAPI, $objOptin, $objIntegration, $arrData)
    {

        // API Daten abholen
        $strAPIKey = $objAPI->api_key;
        $strAPISecret = $objAPI->private_key;
        $strEmail = $objOptin->email;

        // Mehrfachauswahl-Feld (checkboxen) abholen und die deaktivierung der "nicht angewaehlten" Optionen vorbereiten
        $arrDisableOptions = array();
        if ($colFormFields = FormFieldModel::findByPid($objIntegration->form_id)) {

            foreach ($colFormFields as $objFormfield) {
                if ($objFormfield->type == 'checkbox') {

                    if ($arrOptions = unserialize($objFormfield->options)) {

                        foreach ($arrOptions as $arrOption) {

                            $strFieldname = $objFormfield->name;
                            if ($objFormfield->mailjetfieldname != '') {
                                $strFieldname = $objFormfield->mailjetfieldname;
                            }

                            $arrDisableOptions[$strFieldname][$arrOption['value']] = $arrOption['label'];

                        }

                    }
                }
            }

        }

        // Mit API von MailJet verbinden
        if ($objMailjet = self::getMailjetClient($objAPI)) {

            // Besteht der Kontakt schon?
            if ($bolContactExists = self::checkContactExist($objAPI, $strEmail)) {

                // Kontakt Eigenschaften aufbereiten
                if (is_array($arrData) && count($arrData) > 0) {

                    foreach ($arrData as $strField => $strValue) {

                        if (array_key_exists($strField, $arrDisableOptions) && $arrDisableOptions[$strField]) {

                            if (is_array($strValue)) {

                                // Aktiviere Felder, die angewählt wurden (und entferne diese aus dem zu deaktivieren-Optionen Array)
                                foreach ($strValue as $strOption) {

                                    if ($arrDisableOptions[$strField][$strOption]) {
                                        unset($arrDisableOptions[$strField][$strOption]);
                                    }

                                    $arrBody['Data'][] = array(
                                        'Name' => $strOption,
                                        'value' => 1
                                    );

                                }

                                // Deaktiviere Felder, die nicht angewählt wurden
                                foreach ($arrDisableOptions[$strField] as $strOption => $strOptionLabel) {

                                    $arrBody['Data'][] = array(
                                        'Name' => $strOption,
                                        'value' => 0
                                    );

                                }
                            } else {

                                if (is_array($arrDisableOptions[$strField])) {

                                    // Aktiviere Felder, die nicht angewählt wurden (alle Themen ausgewählt)
                                    foreach ($arrDisableOptions[$strField] as $strOption => $strOptionLabel) {

                                        $arrBody['Data'][] = array(
                                            'Name' => $strOption,
                                            'value' => 1
                                        );

                                    }

                                }

                            }

                        } elseif ($strField != 'email') {

                            $arrBody['Data'][] = array(
                                'Name' => $strField,
                                'value' => $strValue
                            );

                        }
                    }

                } else {

                    throw new Exception("No or wrong data-array was passed to the addContactProperty method.");

                }

                // Kontakt Eigenschaften importieren
                $objMailjet = new Client($strAPIKey, $strAPISecret, true, ['version' => 'v3', 'connect_timeout' => 20]);
                $objResponse = $objMailjet->put(Resources::$Contactdata, ['id' => $strEmail, 'body' => $arrBody]);

                // Wurden die Eigeschaften importiert?
                if (!$objResponse->success()) {
                    $arrResponse = $objResponse->getData();

                    throw new Exception("Newsletter-Contact could not be updated: " . $arrResponse['ErrorMessage']);

                } else {

                    return true;

                }

            } else {

                throw new Exception("Contact does not exist: " . $strEmail);

            }

        } else {

            throw new Exception("Mailjet API URL or Login wrong");

        }

        return false;
    }

    public static function sendOptInMail($objRecipient, $objAPI, $objIntegration, $strURL)
    {

        // Mit API von MailJet verbinden
        $objMailjet = self::getMailjetClient($objAPI, 'v3.1');

        // Name auslesen, wenn vorhanden
        $arrData = unserialize($objRecipient->formfields);
        if (array_key_exists('name', $arrData) && $arrData['name'] != '') {

            $strName = $arrData['name'];

        } elseif (array_key_exists('firstname', $arrData) && array_key_exists('lastname', $arrData) && $arrData['firstname'] != '' && $arrData['lastname'] != '') {

            $strName = $arrData['firstname'] . ' ' . $arrData['lastname'];

        } else {

            $strName = $objRecipient->email;

        }

        // Email auslesen, wenn vorhanden
        if ($objRecipient->email) {

            $strEmail = $objRecipient->email;

        } else {

            throw new Exception("No E-Mail address defined for the Opt-In with ID: " . $objRecipient->id);

        }

        // Transaktionales E-Mail Template vorbereiten
        if ($objIntegration->sender_address != '') {
            $strSenderMail = $objIntegration->sender_address;
        } else {
            $strSenderMail = $objIntegration->sender_email;
        }

        $objTemplate = MailJetTransactionalTemplatesModel::findByPk($objIntegration->mail_template_id);
        $arrBody = [
            'Messages' => [
                [
                    'From' => [
                        'Email' => $strSenderMail,
                        'Name' => $objIntegration->sender_name
                    ],
                    'To' => [
                        [
                            'Email' => "$strEmail",
                            'Name' => "$strName"
                        ]
                    ],
                    'TemplateID' => (int)$objTemplate->template_id,
                    'TemplateLanguage' => true,
                    'Subject' => $objIntegration->subject,
                    'Variables' => json_decode('{"salutation": "' . $objRecipient->salutation . '","firstname": "' . $objRecipient->firstname . '","lastname": "' . $objRecipient->lastname . '","optin-url": "' . $strURL . '"}', true)
                ]
            ]
        ];

        // Transaktionales Opt-In Mail verschicken
        $objResponse = $objMailjet->post(Resources::$Email, ['body' => $arrBody]);

        // Wurden die Eigeschaften importiert?
        if (!$objResponse->success()) {
            $arrResponse = $objResponse->getData();

            throw new Exception("Newsletter-Contact could not be updated: " . $arrResponse['ErrorMessage']);

        } else {

            return true;

        }
    }

    public static function sendTransactionalMail($objTemplate, $strEmail, $arrFormData, $arrAttachements)
    {

        // Get Logger
        $objLogger = System::getContainer()->get('monolog.logger.contao');

        // Check if the API exists?
        if ($objAPI = MailJetAPIModel::findByPk($objTemplate->api_id)) {

            // Mit API von MailJet verbinden
            if ($objMailjet = self::getMailjetClient($objAPI, 'v3.1')) {

                //Prepare Date for Body
                $strSenderName = $objTemplate->sender_name;
                $strSenderEmail = $objTemplate->sender_email;

                // Transaktionales E-Mail Template vorbereiten
                $arrBody = [
                    'Messages' => [
                        [
                            'From' => [
                                'Email' => "$strSenderEmail",
                                'Name' => "$strSenderName"
                            ],
                            'To' => [
                                [
                                    'Email' => "$strEmail",
                                ]
                            ],
                            'TemplateID' => (int)$objTemplate->template_id,
                            'TemplateLanguage' => true,
                            'TemplateErrorReporting' => [
                                'Email' => "zuend@mediamotion.ch",
                                'Name' => "Rory Zünd"
                            ],
                            'Subject' => $objTemplate->subject,
                            'Variables' => $arrFormData,
                            'Attachments' => $arrAttachements
                        ]
                    ]
                ];

                // Transaktionales Opt-In Mail verschicken
                $objResponse = $objMailjet->post(Resources::$Email, ['body' => $arrBody]);

                // Wurden das E-Mail verschickt?
                if ($objResponse->success()) {
                    return true;
                } else {
                    $arrResponse = $objResponse->getData();
                    $objLogger->log(LogLevel::ERROR, "Transactional Mail could not be sent: " . json_encode($arrResponse), array('contao' => new ContaoContext(__FUNCTION__, __CLASS__)));
                }
            } else {
                $objLogger->log(LogLevel::ERROR, "Could not connect to API-ID: " . $objAPI->id, array('contao' => new ContaoContext(__FUNCTION__, __CLASS__)));
            }
        } else {
            $objLogger->log(LogLevel::ERROR, "API with the ID: " . $objTemplate->api_id . " does not exist!", array('contao' => new ContaoContext(__FUNCTION__, __CLASS__)));
        }

        return false;
    }

    public static function addContactToList($objAPI, $intContactListID, $strEmail)
    {

        // Mit API von MailJet verbinden
        $objMailjet = self::getMailjetClient($objAPI);

        // Daten aufbereiten
        $arrBody = [
            'ContactsLists' => [
                [
                    'ListID' => "$intContactListID",
                    'Action' => "addforce"
                ]
            ]
        ];

        // User in Kontaktliste aufnehmen
        $objResponse = $objMailjet->post(Resources::$ContactManagecontactslists, ['id' => $strEmail, 'body' => $arrBody]);

        if (!$objResponse->success()) {
            $arrResponse = $objResponse->getData();

            throw new Exception("Newsletter-Contact could not be added to contactlist: " . print_r($arrResponse, true));

        } else {

            return true;

        }
    }

    public static function removeContactFromAllLists($objAPI, $strEmail)
    {

        // Mit API von MailJet verbinden
        $objMailjet = self::getMailjetClient($objAPI);

        // Alle Kontaktlisten holen
        if ($colContactlists = MailJetContactlistModel::findBy(array('published=?'), array(1))) {
            foreach ($colContactlists as $objContactlist) {
                $arrBody['ContactsLists'][] = array(
                    'ListID' => "$objContactlist->contactlist_id",
                    'Action' => "unsub"
                );
            }
        }

        // User in Kontaktliste aufnehmen
        $objResponse = $objMailjet->post(Resources::$ContactManagecontactslists, ['id' => $strEmail, 'body' => $arrBody]);

        if (!$objResponse->success()) {
            $arrResponse = $objResponse->getData();

            throw new Exception("Newsletter-Contact could not be added to contactlist: " . print_r($arrResponse, true));

        } else {

            return true;

        }
    }


    public static function sendMails()
    {

        // Disable PHP Timeout (for large massmailings)
        ini_set('max_execution_time', 0);
        set_time_limit(0);

        // Get GET Parameter
        $intId = Input::get('id');

        // If GET Parameter is set
        if ($intId) {

            if ($objMassMail = MailJetMassmailingModel::findByPk($intId)) {

                $strPageURL = System::getContainer()->get('contao.insert_tag.parser')->replace($objMassMail->opt_out_page);
                $strBaseURL = System::getContainer()->get('contao.insert_tag.parser')->replace('{{env::path}}') . $strPageURL;

                if ($objMassMail->opt_in_page) {
                    $strOptInPageURL = System::getContainer()->get('contao.insert_tag.parser')->replace($objMassMail->opt_in_page);
                    $strBaseOptInURL = System::getContainer()->get('contao.insert_tag.parser')->replace('{{env::path}}') . $strOptInPageURL;
                }

                if ($objMassMail->opt_out_page && $strBaseURL != '') {

                    if ($objAPI = MailJetAPIModel::findByPk($objMassMail->api_id)) {

                        // Mit API von MailJet verbinden
                        if ($objMailjet = self::getMailjetClient($objAPI, 'v3.1')) {

                            $colRecipients = MailJetRecipientsModel::findBy(array('tl_mailjet_recipients.pid=?', 'tl_mailjet_recipients.sent=?', 'tl_mailjet_recipients.published=?'), array($objMassMail->recipient_archiv_id, 0, 1));

                            if ($colRecipients) {

                                unset($arrBody);
                                $arrBody = array();
                                unset($arrSent);
                                $arrSent = array();
                                $intCounter = 0;
                                $intFailCounter = 0;
                                $intMailJetCounter = 0;
                                $intMailJetRepeatCount = 0;
                                $bolSuccess = true;
                                unset($arrUserIDs);
                                $arrUserIDs = array();

                                foreach ($colRecipients as $intKey => $objRecipient) {

                                    $strEmail = $objRecipient->email;
                                    $strName = $objRecipient->firstname . " " . $objRecipient->lastname;

                                    if (stristr($strEmail, '@') && stristr($strEmail, ' ')) {
                                        $strEmail = str_replace(' ', '', $strEmail);
                                    }

                                    if (filter_var($strEmail, FILTER_VALIDATE_EMAIL)) {

                                        if (!$objRecipient->url_email || !$objRecipient->hash) {
                                            $strURLEmail = urlencode(str_replace('@', '-', $strEmail));
                                            $strHash = md5($strURLEmail . time() . 'mailjet-0pt1n-Code');

                                            $objRecipient->url_email = $strURLEmail;
                                            $objRecipient->hash = $strHash;

                                            $objRecipient->save();
                                        }

                                        $strOptInURL = "no-page-or-integration-set";
                                        if ($objMassMail->opt_in_page && $objMassMail->integration_id) {

                                            $objOptIn = new MailJetOptinModel();
                                            $objOptIn->newOptIn($strEmail, array(), $objMassMail->integration_id);
                                            $objOptIn->__set('sent', 1);
                                            $objOptIn->sent_tstamp = time();
                                            $objOptIn->save();

                                            $strOptInURL = $strBaseOptInURL . "?mail=" . $objOptIn->email_url . "&hash=" . $objOptIn->hash;

                                        }

                                        $strURL = $strBaseURL . "?mail=" . $objRecipient->url_email . "&hash=" . $objRecipient->hash;

                                        $strVariable1 = ($objRecipient->variable1 != NULL) ? $objRecipient->variable1 : "";
                                        $strVariable2 = ($objRecipient->variable2 != NULL) ? $objRecipient->variable2 : "";
                                        $strVariable3 = ($objRecipient->variable3 != NULL) ? $objRecipient->variable3 : "";
                                        $strVariable4 = ($objRecipient->variable4 != NULL) ? $objRecipient->variable4 : "";
                                        $strVariable5 = ($objRecipient->variable5 != NULL) ? $objRecipient->variable5 : "";
                                        $strVariable6 = ($objRecipient->variable6 != NULL) ? $objRecipient->variable6 : "";
                                        $strVariable7 = ($objRecipient->variable7 != NULL) ? $objRecipient->variable7 : "";
                                        $strVariable8 = ($objRecipient->variable8 != NULL) ? $objRecipient->variable8 : "";
                                        $strVariable9 = ($objRecipient->variable9 != NULL) ? $objRecipient->variable9 : "";
                                        $strVariable10 = ($objRecipient->variable10 != NULL) ? $objRecipient->variable10 : "";

                                        $objMailTemplate = MailJetTransactionalTemplatesModel::findByPk($objMassMail->mail_template_id);

                                        // Transaktionales E-Mail Template vorbereiten
                                        $arrSent[] = $intKey;
                                        $arrBody['Messages'][] = [
                                            'From' => [
                                                'Email' => $objMassMail->sender_email,
                                                'Name' => $objMassMail->sender_name
                                            ],
                                            'To' => [
                                                [
                                                    'Email' => "$strEmail",
                                                    'Name' => "$strName"
                                                ]
                                            ],
                                            'TemplateID' => (int)$objMailTemplate->template_id,
                                            'TemplateLanguage' => true,
                                            'Subject' => $objMassMail->subject,
                                            'Variables' => json_decode('{"name": "' . $strName . '","optinurl": "' . $strOptInURL . '","optouturl": "' . $strURL . '","variable1": "' . $strVariable1 . '","variable2": "' . $strVariable2 . '","variable3": "' . $strVariable3 . '","variable4": "' . $strVariable4 . '","variable5": "' . $strVariable5 . '","variable6": "' . $strVariable6 . '","variable7": "' . $strVariable7 . '","variable8": "' . $strVariable8 . '","variable9": "' . $strVariable9 . '","variable10": "' . $strVariable10 . '"}', true)
                                        ];

                                        $arrUserIDs[$objRecipient->id] = 'User ID: ' . $objRecipient->id . " - " . $strEmail;

                                        $intCounter++;
                                        $intMailJetCounter++;

                                        //Max 50 Recipients pro Call
                                        if ($intMailJetCounter >= 49) {

                                            $intMailJetCounter = 0;
                                            $intMailJetRepeatCount++;

                                            $bolResult = self::sendMassmails($objMailjet, $arrBody, $objMassMail, $intMailJetRepeatCount);

                                            unset($arrBody);
                                            $arrBody = array();

                                            System::getContainer()
                                                ->get('monolog.logger.contao')
                                                ->log(LogLevel::INFO, "Massenmail Block: " . $intMailJetRepeatCount . " mit folgenden Empfängern: " . implode(", ", $arrUserIDs), array(
                                                    'contao' => new ContaoContext(__CLASS__ . '::' . __FUNCTION__, TL_GENERAL
                                                    )));

                                            unset($arrUserIDs);
                                            $arrUserIDs = array();

                                            if (!$bolResult) {

                                                Message::addError("E-Mails konnten nicht verschickt werden. Fehler im " . $intMailJetRepeatCount . ". Block. Mehr dazu im System-Log.");

                                                $bolSuccess = false;

                                                System::getContainer()
                                                    ->get('monolog.logger.contao')
                                                    ->log(LogLevel::ERROR, "Massenmail Block: " . $intMailJetRepeatCount . " Fehler.", array(
                                                        'contao' => new ContaoContext(__CLASS__ . '::' . __FUNCTION__, TL_ERROR
                                                        )));

                                                break;

                                            } else {

                                                //Verschickte Empfänger als verschickt markieren (verhindern von doppelter Zustellung)
                                                foreach ($arrSent as $intSentKey) {

                                                    $colRecipients[$intSentKey]->sent = 1;
                                                    $colRecipients[$intSentKey]->save();

                                                }

                                                unset($arrSent);
                                                $arrSent = array();

                                                System::getContainer()
                                                    ->get('monolog.logger.contao')
                                                    ->log(LogLevel::INFO, "Massenmail Block: " . $intMailJetRepeatCount . " erfolgreich.", array(
                                                        'contao' => new ContaoContext(__CLASS__ . '::' . __FUNCTION__, TL_GENERAL
                                                        )));

                                            }

                                        }
                                    } else {

                                        System::getContainer()
                                            ->get('monolog.logger.contao')
                                            ->log(LogLevel::ERROR, "Massenmail Fehler: User ID - " . $objRecipient->id . " hat keine gültige E-Mail Adresse: " . $objRecipient->email, array(
                                                'contao' => new ContaoContext(__CLASS__ . '::' . __FUNCTION__, TL_ERROR
                                                )));

                                    }
                                }

                                //Sofern bisher alles ok war, weitermachen mit dem Rest
                                if ($bolSuccess) {

                                    //Restlichen Mails verschicken (unter 50)
                                    if ($intMailJetCounter > 0) {

                                        sleep(1);

                                        $intMailJetRepeatCount++;

                                        $bolResult = self::sendMassmails($objMailjet, $arrBody, $objMassMail, $intMailJetRepeatCount);

                                        System::getContainer()
                                            ->get('monolog.logger.contao')
                                            ->log(LogLevel::INFO, "Massenmail Block: " . $intMailJetRepeatCount . " mit folgenden Empfängern: " . implode(", ", $arrUserIDs), array(
                                                'contao' => new ContaoContext(__CLASS__ . '::' . __FUNCTION__, TL_GENERAL
                                                )));

                                        unset($arrUserIDs);
                                        $arrUserIDs = array();

                                        if (!$bolResult) {

                                            Message::addError("E-Mails konnten nicht verschickt werden. Fehler im letztem Block. Mehr dazu im System-Log.");

                                            $bolSuccess = false;

                                            System::getContainer()
                                                ->get('monolog.logger.contao')
                                                ->log(LogLevel::ERROR, "Massenmail Block: " . $intMailJetRepeatCount . " Fehler.", array(
                                                    'contao' => new ContaoContext(__CLASS__ . '::' . __FUNCTION__, TL_ERROR
                                                    )));

                                        } else {

                                            //Verschickte Empfänger als verschickt markieren (verhindern von doppelter Zustellung)
                                            foreach ($arrSent as $intSentKey) {

                                                $colRecipients[$intSentKey]->sent = 1;
                                                $colRecipients[$intSentKey]->save();

                                            }

                                            unset($arrSent);
                                            $arrSent = array();

                                            System::getContainer()
                                                ->get('monolog.logger.contao')
                                                ->log(LogLevel::INFO, "Massenmail Block: " . $intMailJetRepeatCount . " erfolgreich.", array(
                                                    'contao' => new ContaoContext(__CLASS__ . '::' . __FUNCTION__, TL_GENERAL
                                                    )));

                                        }
                                    }

                                    //Restlicher Versand ok?
                                    if ($bolSuccess) {

                                        if ($intMailJetRepeatCount > 1) {
                                            Message::addConfirmation("Massenmail wurde mit " . $intMailJetRepeatCount . " API-Aufrufen erfolgreich an <b>" . $intCounter . "</b> Empfänger verschickt.");
                                        } else {
                                            Message::addConfirmation("Massenmail wurde mit einem API-Aufruf erfolgreich an <b>" . $intCounter . "</b> Empfänger verschickt.");
                                        }

                                        Message::addInfo("Für Statistiken, bitte auf <a href='https://app.mailjet.com' target='mailjet'>www.mailjet.de</a> gehen.");

                                        if ($intFailCounter == 1) {

                                            Message::addInfo("<b>" . $intFailCounter . "</b> Empfänger wurde nicht angeschrieben, da dieser abgemeldet/deaktiviert ist.");

                                        } elseif ($intFailCounter > 1) {

                                            Message::addInfo("<b>" . $intFailCounter . "</b> Empfänger wurden nicht angeschrieben, da diese abgemeldet/deaktiviert sind.");
                                        }

                                        $objMassMail->sent = 1;
                                        $objMassMail->tstamp_sent = time();
                                        $objMassMail->save();

                                    }
                                }

                            } else {
                                Message::addError("Massenmail Empfänger konnten nicht gefunden werden, kontrollieren Sie die hinterlegte Empfängergruppe");
                            }

                        } else {
                            Message::addError("MailJet Verbindung war nicht erfolgreich, kontrollieren Sie Ihre API-Keys");
                        }

                    } else {
                        Message::addError("API wurde nicht gefunden, kontrollieren Sie die hinterlegte API");
                    }
                } else {
                    Message::addError("Abmeldeseite wurde nicht gefunden, kontrollieren Sie die hinterlegte Seite");
                }

            } else {
                Message::addError("Massenmail wurde nicht gefunden");
            }

        } else {
            Message::addError("Kein Massenmail definiert");
        }

        Controller::redirect('/contao?do=mailjet-massmailing');

    }

    public static function sendMassmails($objMailjet, $arrBody, $objMassMail, $intBlockCount)
    {

        //Daten vorhanden?
        if (!empty($arrBody)) {

            // Transaktionales Opt-In Mail verschicken
            $objResponse = $objMailjet->post(Resources::$Email, ['body' => $arrBody]);

            // Wurden die Eigeschaften importiert?
            if (!$objResponse->success()) {

                $arrResponse = $objResponse->getData();
                $strResponse = json_encode($arrResponse);

                System::getContainer()
                    ->get('monolog.logger.contao')
                    ->log(LogLevel::INFO, 'Massenmail Block Nr.: ' . $intBlockCount . ' konnte nicht verschickt werden: ' . $strResponse, array(
                        'contao' => new ContaoContext(__CLASS__ . '::' . __FUNCTION__, TL_GENERAL
                        )));

                return false;

            } else {

                return true;

            }

        } else {

            System::getContainer()
                ->get('monolog.logger.contao')
                ->log(LogLevel::INFO, 'Massenmail Block Nr.: ' . $intBlockCount . ' konnte nicht verschickt werden: Keine Daten im $arrBody', array(
                    'contao' => new ContaoContext(__CLASS__ . '::' . __FUNCTION__, TL_GENERAL
                    )));

            return false;

        }

    }

    public static function syncTemplates()
    {
        // Alle APIs abholen und dann für jede API die Templates holen.
        if ($colAPIs = MailJetAPIModel::findAll()) {

            $arrCurrentTemplates = array();

            foreach ($colAPIs as $objAPI) {

                // Mit API von MailJet verbinden
                $objMailjet = self::getMailjetClient($objAPI);

                // Templates holen (max. 1000)
                $arrFilters = [
                    'Limit' => 1000,
                ];

                $objResponse = $objMailjet->get(Resources::$Template, ['filters' => $arrFilters]);

                // Templates vorhanden
                if ($objResponse->success()) {
                    $arrResponses = $objResponse->getData();
                    $intNewCounter = 0;
                    $intSameCounter = 0;
                    $intChangedCounter = 0;

                    // Pro Template durchlaufen
                    foreach ($arrResponses as $arrTemplate) {

                        // Is the template published?
                        if (in_array('transactional', $arrTemplate['Purposes'])) {

                            // Besteht das Template bereits?
                            if ($objTemplate = MailJetTransactionalTemplatesModel::findBy(array('template_id=?', 'api_id=?'), array($arrTemplate['ID'], $objAPI->id))) {

                                $arrCurrentTemplates[$objTemplate->id] = $objTemplate;

                                // Get the Template-Details
                                if ($arrTemplateDetails = self::getTemplateDetail($objTemplate)) {
                                    // Prepare the Variables for comparing and importing
                                    $strSubject = $arrTemplateDetails['Headers']['Subject'];
                                    $strSenderName = $arrTemplateDetails['Headers']['SenderName'];
                                    $strSenderEmail = $arrTemplateDetails['Headers']['SenderEmail'];
                                    $strHTML = $arrTemplateDetails['Html-part'];

                                    // Ist alles noch gleich
                                    if ($objTemplate->orig_title == $arrTemplate['Name'] && $objTemplate->template_code == $strHTML && $objTemplate->subject == $strSubject && $objTemplate->sender_name == $strSenderName && $objTemplate->sender_email == $strSenderEmail) {

                                        $intSameCounter++;

                                        // Oder hat sich was geändert
                                    } else {
                                        // Only overwrite the "title", if still the same as it used to be
                                        if ($objTemplate->orig_title == $objTemplate->title) {
                                            $objTemplate->title = $arrTemplate['Name'];
                                        }

                                        // Get the variables from the html-markup
                                        preg_match_all('/\{\{(.*?)\}\}/', $strHTML, $arrVariables);
                                        unset($arrVariableList);
                                        $arrVariableList = array();

                                        if (count($arrVariables[1]) > 0) {
                                            foreach ($arrVariables[1] as $strVariable) {
                                                $arrVariable = explode(":", $strVariable);
                                                $strVariableName = $arrVariable[1];
                                                $arrVariableList[] = $strVariableName;
                                            }

                                            $objTemplate->variables = implode(',', $arrVariableList);
                                        }

                                        $objTemplate->template_code = $strHTML ?? '';
                                        $objTemplate->subject = $strSubject ?? '';
                                        $objTemplate->sender_name = $strSenderName ?? '';
                                        $objTemplate->sender_email = $strSenderEmail ?? '';
                                        $objTemplate->orig_title = $arrTemplate['Name'] ?? '';
                                        $objTemplate->save();

                                        $intChangedCounter++;

                                    }
                                } else {
                                    Message::addError("Fehler Abrufen der Template-Details für das Template: <b>" . $objTemplate->title . "</b>");
                                }

                                // Oder ist es ein neues Template?
                            } else {

                                $intNewCounter++;
                                unset($objTransactionalTemplate);
                                $objTransactionalTemplate = new MailJetTransactionalTemplatesModel();
                                $objTransactionalTemplate->tstamp = time();
                                $objTransactionalTemplate->published = 1;
                                $objTransactionalTemplate->api_id = $objAPI->id;
                                $objTransactionalTemplate->title = $arrTemplate['Name'];
                                $objTransactionalTemplate->orig_title = $arrTemplate['Name'];
                                $objTransactionalTemplate->template_id = $arrTemplate['ID'];
                                $objTransactionalTemplate->save();

                                $arrCurrentTemplates[$objTransactionalTemplate->id] = $objTransactionalTemplate;

                                // Get the Template-Details
                                if ($arrTemplateDetails = self::getTemplateDetail($objTransactionalTemplate)) {
                                    $objTransactionalTemplate->template_code = $strHTML;
                                    $objTransactionalTemplate->subject = $strSubject;
                                    $objTransactionalTemplate->sender_name = $strSenderName;
                                    $objTransactionalTemplate->sender_email = $strSenderEmail;
                                    $objTransactionalTemplate->save();

                                } else {
                                    Message::addError("Fehler Abrufen der Template-Details für das Template: <b>" . $objTransactionalTemplate->title . "</b>");
                                }
                            }
                        }
                    }

                    $strMessage = "<b>" . $objAPI->title . "</b>: ";

                    if ($intNewCounter > 0) {
                        $strMessage .= $intNewCounter . " neue Templates, ";
                    }

                    if ($intChangedCounter > 0) {
                        $strMessage .= $intChangedCounter . " aktualisierte Templates, ";
                    }

                    if ($intSameCounter > 0) {
                        $strMessage .= $intSameCounter . " unveränderte Templates, ";
                    }

                    $strMessage = substr($strMessage, 0, -2);

                    Message::addConfirmation($strMessage);

                } else {

                    Message::addError("Fehler beim Abruf der Templates mit der API: <b>" . $objAPI->title . "</b>");

                }
            }

            if (count($arrCurrentTemplates) > 0) {

                $intDeletedCounter = 0;

                $colContaoTemplates = MailJetTransactionalTemplatesModel::findAll();
                foreach ($colContaoTemplates as $objTemplate) {

                    if (!$arrCurrentTemplates[$objTemplate->id] && $objTemplate->published == 1) {

                        $intDeletedCounter++;
                        $objTemplate->published = 0;
                        $objTemplate->save();

                    }

                }

                if ($intDeletedCounter > 0) {
                    Message::addInfo("<b>" . $intDeletedCounter . "</b> Templates wurden deaktiviert.");
                }

            }

        } else {
            Message::addError("Keine Mailjet-API definiert!");
        }

        Controller::redirect('/contao?do=mailjet-templates');
    }

    public static function getTemplateDetail($objTemplate)
    {

        // Check if the API exists?
        if ($objAPI = MailJetAPIModel::findByPk($objTemplate->api_id)) {
            // Try and get the Template-Details
            $objMailjet = self::getMailjetClient($objAPI);
            $objResponse = $objMailjet->get(Resources::$TemplateDetailcontent, ['id' => $objTemplate->template_id]);

            // Result OK?
            if ($objResponse->success()) {
                //Return the Result as Array
                $arrResponse = $objResponse->getData();
                return $arrResponse[0];

            }
        }
        return false;
    }

    public static function syncContactlists()
    {
        // Alle APIs abholen und dann für jede API die Templates holen.
        if ($colAPIs = MailJetAPIModel::findAll()) {

            $arrCurrentContactlists = array();

            foreach ($colAPIs as $objAPI) {

                // Mit API von MailJet verbinden
                $objMailjet = self::getMailjetClient($objAPI);

                // Templates holen (max. 1000)
                $arrFilters = [
                    'Limit' => 1000,
                ];

                $objResponse = $objMailjet->get(Resources::$Contactslist, ['filters' => $arrFilters]);

                // Templates vorhanden
                if ($objResponse->success()) {
                    $arrResponses = $objResponse->getData();
                    $intNewCounter = 0;
                    $intSameCounter = 0;
                    $intChangedCounter = 0;

                    // Pro Kontaktliste durchlaufen
                    foreach ($arrResponses as $arrContactlist) {

                        // Wurde die Kontaktliste evtl. gelöscht
                        if ($arrContactlist['IsDeleted'] === false) {

                            // Besteht die Kontaktliste bereits?
                            if ($objContactlist = MailJetContactlistModel::findBy(array('contactlist_id=?', 'api_id=?'), array($arrContactlist['ID'], $objAPI->id))) {

                                $arrCurrentContactlists[$objContactlist->id] = $objContactlist;

                                if ($objContactlist->orig_title == $arrContactlist['Name']) {

                                    $intSameCounter++;

                                    // Oder hat sich was geändert
                                } else {

                                    // Only overwrite the "title", if still the same as it used to be
                                    if ($objContactlist->orig_title == $objContactlist->title) {
                                        $objContactlist->title = $arrContactlist['Name'];
                                    }

                                    $objContactlist->orig_title = $arrContactlist['Name'];
                                    $objContactlist->save();

                                    $intChangedCounter++;

                                }


                                // Oder ist es ein neues Template?
                            } else {

                                $intNewCounter++;
                                unset($objNewContactlist);

                                $objNewContactlist = new MailJetContactlistModel();
                                $objNewContactlist->tstamp = time();
                                $objNewContactlist->published = 1;
                                $objNewContactlist->api_id = $objAPI->id;
                                $objNewContactlist->title = $arrContactlist['Name'];
                                $objNewContactlist->orig_title = $arrContactlist['Name'];
                                $objNewContactlist->contactlist_id = $arrContactlist['ID'];
                                $objNewContactlist->save();

                                $arrCurrentContactlists[$objNewContactlist->id] = $objNewContactlist;

                            }
                        }
                    }

                    $strMessage = "<b>" . $objAPI->title . "</b>: ";

                    if ($intNewCounter > 0) {
                        $strMessage .= $intNewCounter . " neue Kontaktlisten, ";
                    }

                    if ($intChangedCounter > 0) {
                        $strMessage .= $intChangedCounter . " aktualisierte Kontaktlisten, ";
                    }

                    if ($intSameCounter > 0) {
                        $strMessage .= $intSameCounter . " unveränderte Kontaktlisten, ";
                    }

                    $strMessage = substr($strMessage, 0, -2);

                    Message::addConfirmation($strMessage);

                } else {

                    Message::addError("Fehler beim Abruf der Kontaktlisten mit der API: <b>" . $objAPI->title . "</b>");

                }
            }

            if (count($arrCurrentContactlists) > 0) {

                $intDeletedCounter = 0;

                $colContaoContactlists = MailJetContactlistModel::findAll();
                foreach ($colContaoContactlists as $objContactlist) {

                    if (!array_key_exists($objContactlist->id, $arrCurrentContactlists) && $objContactlist->published == 1) {

                        $intDeletedCounter++;
                        $objContactlist->published = 0;
                        $objContactlist->save();

                    }

                }

                if ($intDeletedCounter > 0) {
                    Message::addInfo("<b>" . $intDeletedCounter . "</b> Kontaktlisten wurden deaktiviert.");
                }

            }

        } else {
            Message::addError("Keine Mailjet-API definiert!");
        }

        Controller::redirect('/contao?do=mailjet-contactlists');
    }

    public static function checkTemplateExists($objTemplate)
    {
        // Check if the API exists?
        if ($objAPI = MailJetAPIModel::findByPk($objTemplate->api_id)) {
            // Get to API Client
            if ($objMailjet = self::getMailjetClient($objAPI)) {
                $objResponse = $objMailjet->get(Resources::$Template, ['id' => $objTemplate->template_id]);

                if ($objResponse->success()) {
                    return true;
                } else {
                    throw new Exception("Template with the ID: " . $objTemplate->template_id . " does not exists or could not be fetched.");
                }
            } else {
                throw new Exception("Could not connect to API-ID: " . $objAPI->id);
            }
        } else {
            throw new Exception("API with the ID: " . $objTemplate->api_id . " does not exist!");
        }

        return false;
    }

    public static function checkMailAdress($strEmail)
    {
        // Format Check
        if (filter_var($strEmail, FILTER_VALIDATE_EMAIL)) {

            // DNS Check (optional)
            $bolDNSCheck = Config::get('project_listingpage');

            if ($bolDNSCheck) {

                list (, $strDomain) = explode('@', $strEmail);
                if (checkdnsrr($strDomain, 'MX')) {
                    return true;
                } else {
                    return false;
                }

            }

            return true;
        } else {
            return false;
        }
    }
}
