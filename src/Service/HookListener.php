<?php
/**
 * @package   MailJetBundle
 * @author    Rory Zünd, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

namespace Memo\MailJetBundle\Service;

use Contao\PageModel;

class HookListener
{

    public function getRootPage($objTargetPage)
    {

        if ($objTargetPage->type == 'root') {

            return $objTargetPage;

        } elseif ($objTargetPage->pid) {

            $objParentPage = PageModel::findOneById($objTargetPage->pid);
            return self::getRootPage($objParentPage);

        }
    }
}
