<?php
/**
 * @package   MailJetBundle
 * @author    Rory Zünd, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */


namespace Memo\MailJetBundle\Gateway;

use Contao\CoreBundle\Monolog\ContaoContext;
use Contao\System;
use Mailjet\Client;
use Mailjet\Resources;
use Memo\MailJetBundle\MessageDraft\MailjetMessageDraft;
use Memo\MailJetBundle\Model\MailJetTransactionalTemplatesModel;
use Memo\MailJetBundle\Service\MailJetHandler;
use NotificationCenter\Gateway\Base;
use NotificationCenter\Gateway\GatewayInterface;
use NotificationCenter\MessageDraft\MessageDraftFactoryInterface;
use NotificationCenter\MessageDraft\MessageDraftInterface;
use NotificationCenter\Model\Gateway;
use NotificationCenter\Model\Language;
use NotificationCenter\Model\Message;
use NotificationCenter\Util\StringUtil;
use Psr\Log\LogLevel;

if (!interface_exists('\NotificationCenter\Gateway\GatewayInterface')) {
    return false;
}

class Mailjet extends Base implements GatewayInterface, MessageDraftFactoryInterface
{
    public function send(Message $objMessage, array $arrTokens, $strLanguage = '')
    {
        // Clean up the Tokens-Array
        $arrCleanTokens = [];
        foreach ($arrTokens as $key => $value) {
            if (!preg_match_all('/raw_data/', $key) && !preg_match_all('/formconfig_/', $key) && $value != '' && !is_null($value)) {
                $arrCleanTokens[$key] = $value;
            }
        }

        // Get the Gateway to the Message
        $objGateway = Gateway::findBy('id', $objMessage->gateway);

        // Create MessageDraft
        if ($objDraft = $this->createDraft($objMessage, $arrCleanTokens, $strLanguage)) {
            // Send Draft to Recipients
            if ($this->sendDraft($objDraft, $objGateway)) {
                return true;
            }
        }

        return false;

    }

    /**
     * Creates a MessageDraft
     * @param Message
     * @param array
     * @param string
     * @return  MessageDraftInterface|null (if no draft could be found)
     */
    public function createDraft(Message $objMessage, array $arrTokens, $strLanguage = '')
    {

        // Is there a language set? (otherwise get default-language)
        if ($strLanguage == '') {
            $strLanguage = $GLOBALS['TL_LANGUAGE'];
        }

        // Does the Language-Entry to the message exist?
        if (($objLanguage = Language::findByMessageAndLanguageOrFallback($objMessage, $strLanguage)) === null) {
            System::log(sprintf('Could not find matching language or fallback for message ID "%s" and language "%s".', $objMessage->id, $strLanguage), __METHOD__, TL_ERROR);
            return null;
        }

        return new MailjetMessageDraft($objMessage, $objLanguage, $arrTokens);

    }

    /**
     * @param MailjetMessageDraft $objDraft
     * @return bool
     * @throws \Exception
     */
    public function sendDraft(MessageDraftInterface $objDraft, Gateway $gateway)
    {

        // Get Logger
        $objLogger = System::getContainer()->get('monolog.logger.contao');

        // Get the Tokens (posted Form-Fields)
        $arrTokens = $objDraft->getTokens();

        // Get the Language-Object (Message Child)
        $objLanguage = $objDraft->getLanguageObject();

        // Does the template exist in Contao?
        if ($objMailjetTemplate = MailJetTransactionalTemplatesModel::findByPk($objLanguage->mailjet_template)) {

            // Does the template exist on Mailjet?
            if (MailJetHandler::checkTemplateExists($objMailjetTemplate)) {

                // Get Attachements
                $arrAttachements = [];

                // Add file attachments
                $arrFileAttachments = $objDraft->getAttachments();
                if (!empty($arrFileAttachments)) {
                    $arrAttachements = array_merge($arrAttachements, $arrFileAttachments);
                }
                // Add string attachments
                $arrStringAttachments = $objDraft->getStringAttachments();
                if (!empty($arrStringAttachments)) {
                    $arrAttachements = array_merge($arrAttachements, $arrStringAttachments);
                }

                // Preset field "email" must exist and be valid (by PHP definition)
                $strEmail = $arrTokens['form_email'];
                if (filter_var($strEmail, FILTER_VALIDATE_EMAIL)) {

                    if (MailJetHandler::sendTransactionalMail($objMailjetTemplate, $strEmail, $arrTokens, $arrAttachements)) {
                        $objLogger->log(LogLevel::INFO, 'Transaktionales E-Mail mit Template-ID: ' . $objMailjetTemplate->id . " an " . $strEmail . " verschickt.", array('contao' => new ContaoContext(__FUNCTION__, __CLASS__)));

                        return true;
                    }

                } else {
                    $objLogger->log(LogLevel::ERROR, 'Transactional e-mai not sent, because the e-mail address is invalid: ' . $strEmail, array('contao' => new ContaoContext(__FUNCTION__, __CLASS__)));
                }
            } else {
                $objLogger->log(LogLevel::ERROR, 'Ungültige Mailjet-Template-Vorlage in Benachrichtigung hinterlegt, Template-ID: ' . $objLanguage->mailjet_template . " existiert auf Mailjet nicht.", array('contao' => new ContaoContext(__FUNCTION__, __CLASS__)));
            }

        } else {
            $objLogger->log(LogLevel::ERROR, 'Ungültige Mailjet-Template-Vorlage in Benachrichtigung hinterlegt, Template-ID: ' . $objLanguage->mailjet_template . " existiert im Contao nicht.", array('contao' => new ContaoContext(__FUNCTION__, __CLASS__)));
        }

        return false;

    }
}
