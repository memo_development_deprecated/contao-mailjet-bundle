<?php
/**
 * @package   MailJetBundle
 * @author    Rory Zünd, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

namespace Memo\MailJetBundle\Controller;

use Contao\Controller;
use Contao\Message;
use Memo\MailJetBundle\Model\MailJetRecipientsArchivModel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Contao\System;

/**
 * @Route("/contao/mj-import",
 *  name=BackendController::class,
 *  defaults={
 *       "_scope" = "backend",
 *       "_token_check" = true,
 *       "_backend_module" = "mod_mailjet"
 *  }
 * )
 */
class BackendController extends AbstractController
{

    public function __construct()
    {

        $container = System::getContainer();
        $twig = $container->get('twig');
        $this->twig = $twig;
    }

    public function __invoke()
    {
        $strError = false;

        // Form POST?
        if (isset($_POST['submit'])) {

            $strUploadFolder = __DIR__ . '/../../../../../files/import/';
            $strUploadFile = $strUploadFolder . basename($_FILES["file"]["name"]);

            // Move File to Destination
            if (move_uploaded_file($_FILES["file"]["tmp_name"], $strUploadFile)) {

                // Move ok?
                if (file_exists($strUploadFile)) {
                    $objContainer = System::getContainer();
                    $objImporter = $objContainer->get('memo_mailjetmailjet_importer');

                    $strReturn = $objImporter->importRecipients($strUploadFile, $_POST['group']);

                    if (is_int($strReturn)) {

                        Message::addInfo("<b>" . $strReturn . " Empfänger</b> aus <b>" . $_FILES["file"]["name"] . "</b> erfolgreich importiert.");

                        $strRedirect = urldecode($_GET['redirect']);

                        header("Location: " . $strRedirect);
                        die();
                    } else {
                        $strError = $strReturn;
                    }

                } else {
                    $strError = "Die Datei " . basename($_FILES["file"]["name"]) . " konnte nicht in den Ordner '" . $strUploadFolder . "' hochgeladen werden.";
                }

            } else {
                $strError = "Die Datei " . basename($_FILES["file"]["name"]) . " konnte nicht hochgeladen werden.";
            }
        }

        // Get all Recipient-Groups
        if ($colGroups = MailJetRecipientsArchivModel::findAll()) {
            $bolPreset = false;

            foreach ($colGroups as $objGroup) {
                // Group preset?
                if ($_GET['id'] == $objGroup->id) {
                    $bolPreset = true;
                    $arrGroups[] = array('id' => $objGroup->id, 'name' => $objGroup->title, 'selected' => true);
                } else {
                    $arrGroups[] = array('id' => $objGroup->id, 'name' => $objGroup->title);
                }
            }
        }

        $strRequestToken = System::getContainer()->get('contao.insert_tag.parser')->replace('{{request_token}}');

        $strPath = preg_replace('/Controller$/', '', __DIR__);

        // Return Template
        return new Response($this->twig->render(
            $strPath . 'Resources/views/import_mailjet_recipients.html.twig',
            ['groups' => $arrGroups, 'request_token' => $strRequestToken, 'error' => $strError, 'grouppreset' => $bolPreset]
        ));

    }
}
